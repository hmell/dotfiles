# Dotfiles

This repository uses [stow](https://www.gnu.org/software/stow/) to manage
dotfiles.

Once cloned in home directory as `dotfiles/`:

```bash
cd ~/dotfiles
stow --restow */
```

This will symlink the contents of each top level directory (each *package*
in stow's terminology) into the parent directory, since `--target` is not
specified.

To update/delete all symlinks:

```bash
make
make delete
```

