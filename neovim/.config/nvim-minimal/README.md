# nvim-minimal

Minimal neovim configuration.

A single `init.lua` is used to set options, keymaps, add completion, snippets
and LSP capababilities, etc.

Plugins are installed in a `pack/` directory (not under version control) using
`make install` (requires ansible, available with `devbox shell`). To update
all packages run `update-all`. See `local.yml` and `Makefile` for details.

The specific revisions of each plugin are recorded in the "Plugins" section
below. This is done by running `make`, which itself calls the
`plugin_manager.sh` script.


## Plugins

Last modified: 16/11/24 13:01

[cmp-buffer]
commit: 3022dbc9166796b644a841a02de8dd1cc1d311fa

Last update: 16/11/24 13:01

[cmp-calc]
commit: 5947b412da67306c5b68698a02a846760059be2e

Last update: 16/11/24 13:01

[cmp-emoji]
commit: e8398e2adf512a03bb4e1728ca017ffeac670a9f

Last update: 16/11/24 13:01

[cmp-latex-symbols]
commit: 165fb66afdbd016eaa1570e41672c4c557b57124

Last update: 16/11/24 13:01

[cmp-nvim-lsp]
commit: 39e2eda76828d88b773cc27a3f61d2ad782c922d

Last update: 16/11/24 13:01

[cmp-nvim-lsp-signature-help]
commit: 031e6ba70b0ad5eee49fd2120ff7a2e325b17fa7

Last update: 16/11/24 13:01

[cmp-pandoc-references]
commit: dd86d3a8acc52a6d0503c3a45ba1bb93f39a3d31

Last update: 16/11/24 13:01

[cmp-path]
commit: 91ff86cd9c29299a64f968ebb45846c485725f23

Last update: 16/11/24 13:01

[cmp-spell]
commit: 694a4e50809d6d645c1ea29015dad0c293f019d6

Last update: 16/11/24 13:01

[cmp_luasnip]
commit: 98d9cb5c2c38532bd9bdb481067b20fea8f32e90

Last update: 16/11/24 13:01

[friendly-snippets]
commit: de8fce94985873666bd9712ea3e49ee17aadb1ed

Last update: 16/11/24 13:01

[fzf-lua]
commit: 2a7eb32871a131e24021dd1756865e475fe7e274

Last update: 16/11/24 13:01

[lspkind.nvim]
commit: a700f1436d4a938b1a1a93c9962dc796afbaef4d

Last update: 16/11/24 13:01

[luasnip]
commit: 659c4479529a05cc9b05ef762639a09d366cc690

Last update: 16/11/24 13:01

[mini.nvim]
commit: 7b4d5d48b6b5a75009d63f8f3e4ef4819b7e8139

Last update: 16/11/24 13:01

[nvim-cmp]
commit: f17d9b4394027ff4442b298398dfcaab97e40c4f

Last update: 16/11/24 13:01

[nvim-lspconfig]
commit: 87c7c83ce62971e0bdb29bb32b8ad2b19c8f95d0

Last update: 16/11/24 13:01

[nvim-treesitter]
commit: 20e10ca6914f65cf1410232433fb58de70ab6b39

Last update: 16/11/24 13:01

[nvim-web-devicons]
commit: 19d257cf889f79f4022163c3fbb5e08639077bd8

Last update: 16/11/24 13:01

[otter-nvim]
commit: ca9ce67d0399380b659923381b58d174344c9ee7

Last update: 16/11/24 13:01

[quarto-nvim]
commit: 8e65ef463320545b16cff318d9bb717d003efe81

Last update: 16/11/24 13:01

[tmux-navigate]
commit: 27c83cd229bceff55e6318cce7945995623a6106

Last update: 16/11/24 13:01

[vim-slime]
commit: c8cfb9bec587aef0a7a87a52dacb087f48ea2b41

Last update: 16/11/24 13:01

[vim-slime-cells]
commit: 957027f3bf18796172f2389e9d9d1a01b06f606e
