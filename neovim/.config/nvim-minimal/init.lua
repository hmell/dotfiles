-- learn about neovim's lua api
-- https://neovim.io/doc/user/lua-guide.html

-- _|#options|_

vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.smartcase = true
vim.opt.ignorecase = true
vim.opt.wrap = false
vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.signcolumn = 'yes'
vim.opt.termguicolors = true
vim.opt.colorcolumn = "80"
vim.opt.showtabline = 2
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.mouse = 'a'
vim.cmd [[set iskeyword+=-]]
vim.cmd [[set nomodeline]]

-- _|#keymaps|_

-- space as the leader key
vim.g.mapleader = vim.keycode('<space>')
vim.g.maplocalleader = vim.keycode('<space>')

-- windows and buffers
vim.keymap.set("n", "<c-h>", "<c-w>h")
vim.keymap.set("n", "<c-j>", "<c-w>j")
vim.keymap.set("n", "<c-k>", "<c-w>k")
vim.keymap.set("n", "<c-l>", "<c-w>l")
vim.keymap.set("n", "<s-j>", ":bnext<cr>")
vim.keymap.set("n", "<s-k>", ":bprevious<cr>")
vim.keymap.set("n", "<m-bs>", ":b#<cr>") -- save current buffer
vim.keymap.set("n", "<leader>s", ":silent write<cr>")
vim.keymap.set("n", "<s-x>", ":bdelete<cr>") -- delete current buffer

-- misc
vim.keymap.set("i", "<c-c>", "<esc>")
vim.keymap.set("v", "<c-c>", "<esc>")
vim.keymap.set("n", "<leader>o", "o<esc>")
vim.keymap.set("n", "<leader><s-o>", "o<esc>")
vim.keymap.set("n", "<leader><s-o>", "o<esc>")
vim.keymap.set("n", "<leader>xc", "i# %#% ")
vim.keymap.set("n", "<leader>ci", "o::: {.named-cbl}<CR><CR>:::<ESC>2ko[]{}<ESC>i")

-- basic clipboard interaction
vim.keymap.set({'n', 'x', 'o'}, 'gy', '"+y', {desc = 'copy to clipboard'})
vim.keymap.set({'n', 'x', 'o'}, 'gp', '"+p', {desc = 'paste clipboard text'})

-- command shortcuts
vim.keymap.set('n', '<leader>s', '<cmd>write<cr>', {desc = 'save file'})
vim.keymap.set('n', '<leader>q', '<cmd>quitall<cr>', {desc = 'exit vim'})

-- go template code
-- TODO: replace with a proper snippet
vim.keymap.set('i', '<c-i>e', 'if err != nil {<CR>return err<CR>}<ESC>k$')

-- _|#colorscheme|_
vim.cmd.colorscheme('retrobox-light') -- own customization of retrobox




-- require('mini.completion').setup({})
-- vim.keymap.set('n', '<leader>e', '<cmd>lua minifiles.open()<cr>', {desc = 'file explorer'})
-- require('mini.pick').setup({})

-- _|#lsp::keymaps|_
vim.api.nvim_create_autocmd('LspAttach', {
  desc = 'lsp actions',
  callback = function(event)
    local opts = {buffer = event.buf}

    -- display documentation of the symbol under the cursor
    vim.keymap.set('n', '<leader>k', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)

    -- jump to the definition
    vim.keymap.set('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', opts)

    -- jump to declaration
    vim.keymap.set('n', 'grd', '<cmd>lua vim.lsp.buf.declaration()<cr>', opts)

    -- lists all the implementations for the symbol under the cursor
    vim.keymap.set('n', 'gri', '<cmd>lua vim.lsp.buf.implementation()<cr>', opts)

    -- jumps to the definition of the type symbol
    vim.keymap.set('n', 'grt', '<cmd>lua vim.lsp.buf.type_definition()<cr>', opts)

    -- lists all the references
    vim.keymap.set('n', 'grr', '<cmd>lua vim.lsp.buf.references()<cr>', opts)

    -- renames all references to the symbol under the cursor
    vim.keymap.set('n', 'grn', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)

    -- selects a code action available at the current cursor position
    vim.keymap.set('n', 'gra', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)

    -- displays a function's signature information
    vim.keymap.set('i', '<c-s>', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)

    -- format current file
    vim.keymap.set({'n', 'x'}, 'gq', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
  end,
})


-- _|#autocommands|_
vim.cmd[[
  augroup _general_settings
    autocmd!
    autocmd FileType qf,help,man,lspinfo nnoremap <silent> <buffer> q :close<CR> 
    autocmd TextYankPost * silent!lua require('vim.highlight').on_yank({higroup = 'Visual', timeout = 200}) 
    autocmd BufWinEnter * :set formatoptions-=cro
  augroup end
]]

-- _|#plugins::mini|_
require('mini.bufremove').setup()
require('mini.colors').setup()
require('mini.comment').setup()
require('mini.diff').setup()
require('mini.files').setup()
require('mini.git').setup()
local hipatterns = require('mini.hipatterns')
hipatterns.setup({
  highlighters = {
    -- highlight standalone 'fixme', 'hack', 'todo', 'note'
    fixme = { pattern = '%f[%w]()FIXME()%f[%w]', group = 'MiniHipatternsFixme' },
    hack  = { pattern = '%f[%w]()HACK()%f[%w]',  group = 'MiniHipatternsHack'  },
    todo  = { pattern = '%f[%w]()TODO()%f[%w]',  group = 'MiniHipatternsTodo'  },
    note  = { pattern = '%f[%w]()NOTE()%f[%w]',  group = 'MiniHipatternsNote'  },

    -- highlight hex color strings (`#rrggbb`) using that color
    hex_color = hipatterns.gen_highlighter.hex_color(),
  },
})
require('mini.icons').setup()
require('mini.indentscope').setup()
require('mini.pairs').setup()
require('mini.statusline').setup()
require('mini.surround').setup()
require('mini.tabline').setup()
require('mini.trailspace').setup()
require('mini.visits').setup()



-- _|#treesitter|_
require('nvim-treesitter.configs').setup({ highlight = { enable = true } })

-- _|#plugins::fzf-lua|_
local fzf = require("fzf-lua")
fzf.setup({
  actions = {
    files = {
      ["default"] = fzf.actions.file_edit,
      ["ctrl-s"]  = fzf.actions.file_split,
      ["ctrl-v"]  = fzf.actions.file_vsplit,
      ["ctrl-t"]  = fzf.actions.file_tabedit,
      ["alt-q"]   = fzf.actions.file_sel_to_qf,
    },
  },
})
vim.g.fzf_layout = "{ 'window': { 'width': 0.8, 'height': 0.8 } }"
vim.keymap.set('n', '<leader>fs',  '<cmd>lua require("fzf-lua").blines()<cr>')
vim.keymap.set('n', '<leader>fl',  '<cmd>lua require("fzf-lua").lines()<cr>')
vim.keymap.set('n', '<leader>fp',  '<cmd>lua require("fzf-lua").live_grep()<cr>')
vim.keymap.set('n', '<leader>fc',  '<cmd>lua require("fzf-lua").lines({prompt = "code cells>", fzf_opts = {["--query"] = "%#%", ["--cycle"] = ""}, winopts = { preview = {layout = "vertical"}} })<cr>')
vim.keymap.set('n', '<leader>fgc',  '<cmd>lua require("fzf-lua").git_commits()<cr>')
vim.keymap.set('n', '<leader>fgb',  '<cmd>lua require("fzf-lua").git_bcommits()<cr>')
vim.keymap.set('n', '<leader>fgf',  '<cmd>lua require("fzf-lua").git_files()<cr>')
vim.keymap.set('n', '<leader>ff',  '<cmd>lua require("fzf-lua").files()<cr>')
vim.keymap.set('n', '<leader>fb',  '<cmd>lua require("fzf-lua").buffers()<cr>')
vim.keymap.set('n', '<leader>fh',  '<cmd>lua require("fzf-lua").lsp_document_symbols()<cr>')

-- _|#plugins::nvim-cmp|_
local cmp = require("cmp")
local ls = require("luasnip")
local lspkind = require("lspkind")
-- loads vscode style snippets from installed plugins (e.g. friendly-snippets)
require("luasnip.loaders.from_vscode").lazy_load()
require("luasnip.loaders.from_vscode").lazy_load({ paths = "~/.config/nvim/snippets/vscode-snips" })
require("luasnip.loaders.from_lua").load({paths = "~/.config/nvim/snippets/lua-snips"})
-- keymaps snippets in insert mode
vim.keymap.set({"i", "s"}, "<c-d>", function()
  if ls.expand_or_jumpable() then
    ls.expand_or_jump()
  end
end, {silent = true})
vim.keymap.set({"i", "s"}, "<c-s>", function()
  if ls.jumpable(-1) then
    ls.jump(-1)
  end
end, {silent = true})
vim.keymap.set({"i", "s"}, "<c-o>", function()
  if ls.choice_active() then
    ls.change_choice(1)
  end
end, {silent = true})
cmp.setup({
  completion = {
    completeopt = "menu,menuone,preview,noselect",
  },
  snippet = { -- configure how nvim-cmp interacts with snippet engine
    expand = function(args)
      ls.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    ["<c-b>"] = cmp.mapping.scroll_docs(-4),
    ["<c-f>"] = cmp.mapping.scroll_docs(4),
    ["<c-space>"] = cmp.mapping.complete(), -- show completion suggestions
    ["<c-e>"] = cmp.mapping.abort(), -- close completion window
    ["<cr>"] = cmp.mapping.confirm({ select = false }),
  }),
  -- sources for autocompletion
  sources = cmp.config.sources({
    { name = 'otter' }, -- for code chunks in quarto
    { name = "luasnip" }, -- snippets
    { name = 'path' },
    { name = "nvim_lsp" },
    { name = 'nvim_lsp_signature_help' },
    { name = 'pandoc_references' },
    { name = 'buffer', keyword_length = 3, max_item_count = 3 },
    { name = 'spell' },
    { name = 'treesitter', keyword_length = 3, max_item_count = 3 },
    { name = 'calc' },
    { name = 'latex_symbols' },
    { name = 'emoji' },
  }),
  -- configure lspkind for vs-code like pictograms in completion menu
  formatting = {
    format = lspkind.cmp_format({
      with_text = true,
      menu = {
        otter = "[🦦]",
        luasnip = "[snip]",
        nvim_lsp = "[lsp]",
        buffer = "[buf]",
        path = "[path]",
        spell = "[spell]",
        pandoc_references = "[ref]",
        tags = "[tag]",
        treesitter = "[ts]",
        calc = "[calc]",
        latex_symbols = "[tex]",
        emoji = "[emoji]",
      },
      maxwidth = 50,
      ellipsis_char = "...",
    })
  },
})
-- link quarto and rmarkdown to markdown snippets
ls.filetype_extend("quarto", { "markdown" })
ls.filetype_extend("rmarkdown", { "markdown" })

-- _|#plugins::quarto|_
local quarto = require('quarto')
quarto.setup({
  debug = false,
  closepreviewonexit = true,
  lspfeatures = {
    enabled = true,
    languages = { 'r', 'python', 'bash' },
    diagnostics = {
      enabled = true,
      triggers = { "bufwrite" }
    },
    completion = {
      enabled = true,
    },
  },
})

vim.keymap.set({'n', 'i'}, '<C-i>r', '<esc>i```{r}<cr>```<Esc>O', { desc = '[i]nsert [R] code chunk' })                   vim.keymap.set({'n', 'i'}, '<C-i>p', '<esc>i```{python}<cr>```<Esc>O', { desc = '[i]nsert [p]ython code chunk' })
vim.keymap.set({'n', 'i'}, '<C-i>p', '<esc>i```{python}<cr>```<Esc>O', { desc = '[i]nsert [p]ython code chunk' }) 

-- _|#plugins::vim-slime|_
vim.g.slime_target = "tmux"
vim.g.slime_cell_delimiter = ""
vim.g.slime_default_config = { socket_name = "default", target_pane = "{right-of}" }
vim.g.slime_python_ipython = 1
vim.g.slime_bracketed_paste = 1
vim.g.slime_no_mappings = 1
vim.keymap.set('n', "<C-c><C-c>", "<plug>SlimeParagraphSend", { silent = true, noremap = true })
-- vim-slime-cell keymaps
vim.keymap.set('n', "<leader>cc", "<plug>SlimeCellsSendAndGoToNext", { silent = true, noremap = true })
vim.keymap.set('n', "<leader>cj", "<plug>SlimeCellsNext", { silent = true, noremap = true })
vim.keymap.set('n', "<leader>ck", "<plug>SlimeCellsPrev", { silent = true, noremap = true })

-- _|#lsp::go|_
require('lspconfig').gopls.setup({})

-- _|#lsp::python|_
require('lspconfig').pyright.setup({})

-- _|#lsp::r|_
require('lspconfig').r_language_server.setup({})

-- _|#lsp::julia|_
require('lspconfig').julials.setup({})
