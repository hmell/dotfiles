#!/bin/bash

set -e

PLUGIN_DIR="./pack/dist/start"

get_plugins_revisions ()
{
  d="$1"
  for repo in $(ls "$PLUGIN_DIR")
  do
    plugin="$repo"
    heads_fp="${d}/${plugin}/.git/refs/heads/"
    if [[ -f "${heads_fp}/main" ]]; then
        commit=$(cat "${heads_fp}/main")
    else
        commit=$(cat "${heads_fp}/master")
    fi
    printf "\nLast modified: %s\n" "$(date '+%-d/%-m/%y %H:%M')" >> README.md
    printf "\n[%s]\ncommit: %s\n" "$plugin" "$commit" >> README.md
  done
}

get_plugins_revisions "$PLUGIN_DIR"


main () {
  case "$1" in
  
    -t|--tags)
      shift
      # args: <file with documents info>
      documents_tags "$1"
      ;;
  
    *)
      awk '{print} /## Plugins/ {exit}' README.md > temp && mv temp README.md
      get_plugins_revisions "$PLUGIN_DIR"
      echo "The revisions of all the plugins have been recorded in the README.md!"
      ;;
  esac

}

main "$@"
