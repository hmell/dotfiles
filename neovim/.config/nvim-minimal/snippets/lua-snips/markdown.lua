
return {
    --[[ General ]]--
    -- a snippet for html paragraphs
  require("luasnip").snippet(
    { trig = "<p" },
    { t({"<p>", ""}), i(1), t({"", "</p>"}) }
  ),
    --[[ Tags ]]--
    -- a snippet for infile tags
  require("luasnip").snippet(
    { trig = "itg" },
    { t("_|"), i(1), t("|_") }
  ),
    --[[ Math Typesetting with KaTeX ]]--
    -- a snippet for inline math
  require("luasnip").snippet(
    { trig = "imath" },
    { t("\\("), i(1), t("\\)") }
  ),
    --[[ Citations Hugo documents ]]--
    -- a snippet for citation shortcode
  require("luasnip").snippet(
    { trig = "csh" },
    { t('{{< cite "'), i(1), t('" >}}') }
  ),
    --[[ Code blocks ]]--
    -- a snippet for R code blocks in markdown
  require("luasnip").snippet(
    { trig = "rcb" },
    { t({"```r", ""}), i(1), t({"", "```"}) }
  ),
    --[[ Kukariri cards ]]--
    -- a snippet for audio element
  require("luasnip").snippet(
    { trig = "<au" },

    { t("<audio controls src='.mp3'>"), i(1),
      t("</audio>")}
  ),
}
