return {
  -- A snippet that expands the trigger "hi" into the string "Hello, world!".
  require("luasnip").snippet(
    { trig = "ltx-multi-eq-nonum" },
    { t({"\\begin{align*}", "  &A + B = C \\\\", "  &D \\times E = F", "\\end{align*}" }) }
  ),

  -- To return multiple snippets, use one `return` statement per snippet file
  -- and return a table of Lua snippets.
  require("luasnip").snippet(
    { trig = "foo" },
    { t("Another snippet.") }
  ),
}

