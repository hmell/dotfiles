-- Register new filetype for stan files
vim.filetype.add({ extension = { stan = 'stan' } })
