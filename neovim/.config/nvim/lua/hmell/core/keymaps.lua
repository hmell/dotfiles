vim.g.mapleader = " "
vim.g.maplocalleader = ' '

local opts = {}

vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

vim.keymap.set("x", "<leader>p", [["_dP]])
vim.keymap.set({"n", "v"}, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])
vim.keymap.set({"n", "v"}, "<leader>d", [["_d]])

vim.keymap.set("i", "<C-c>", "<Esc>")
vim.keymap.set("v", "<C-c>", "<Esc>")

-- _|#keybindings::windows|_
vim.keymap.set("n", "<C-h>", "<C-w>h", opts)
vim.keymap.set("n", "<C-j>", "<C-w>j", opts)
vim.keymap.set("n", "<C-k>", "<C-w>k", opts)
vim.keymap.set("n", "<C-l>", "<C-w>l", opts)

-- _|#keybindings::buffers|_
vim.keymap.set("n", "<S-j>", ":bnext<CR>", opts)
vim.keymap.set("n", "<S-k>", ":bprevious<CR>", opts)
vim.keymap.set("n", "<M-BS>", ":b#<CR>", opts) -- save current buffer
vim.keymap.set("n", "<leader>s", ":silent write<CR>", opts)
vim.keymap.set("n", "<S-x>", ":Bdelete<CR>", opts) -- delete current buffer

-- _|#keybindings::edit|_
vim.keymap.set("n", "<leader>o", "o<esc>", opts)
vim.keymap.set("n", "<leader><S-o>", "O<esc>", opts)
vim.keymap.set("v", "<", "<gv", opts)
vim.keymap.set("v", ">", ">gv", opts)
vim.keymap.set("n", "<leader><leader>p", ":lua local t=vim.fn.trim(vim.fn.system('copyq read 0')) ; vim.api.nvim_put({t}, 'c', true, true)<CR><CR>", opts)
vim.keymap.set("n", "<leader><C-p>", ":read !copyq read 0<CR>", opts)
vim.keymap.set("n", "<leader>ci", "o::: {.named-cbl}<CR><CR>:::<ESC>2ko[]{}<ESC>i", opts)


-- _|#keybindings::quickfix|_
vim.keymap.set("n", "<leader>qqu", ":copen<CR>", opts)
vim.keymap.set("n", "<leader>qqd", ":cclose<CR>", opts)
vim.keymap.set("n", "<leader>qqn", ":cnext<CR>", opts)
vim.keymap.set("n", "<leader>qqp", ":cprev<CR>", opts)


-- _|#keybindings::cmds|_

-- Open file under cursor --
vim.cmd [[
  nnoremap gx :execute '! xdg-open ' . shellescape(expand('<cfile>')) .. ' &'<CR>
]]

-- Checkboxes
vim.cmd [[
  nnoremap <Leader>cn i* [ ] <Esc>A
]] -- create checkbox
vim.cmd [[
  nnoremap <Leader>cd 0/[ <CR>lr✓hl
]] -- check checkbox


local diagnostics_active = true
vim.keymap.set('n', '<leader>d', function()
  diagnostics_active = not diagnostics_active
  if diagnostics_active then
    vim.diagnostic.show()
  else
    vim.diagnostic.hide()
  end
end)

-- _|#keybindings::plugins::quarto|_
vim.keymap.set({'n', 'i'}, '<C-i>r', '<esc>i```{r}<cr>```<Esc>O', { desc = '[i]nsert [R] code chunk' })
vim.keymap.set({'n', 'i'}, '<C-i>p', '<esc>i```{python}<cr>```<Esc>O', { desc = '[i]nsert [p]ython code chunk' })

-- _|#keybindings::folding|_
-- Folds --
-- Toggle between folding modes
-- vim.keymap.set("n", "zt", "<cmd>FoldToggle<CR>", opts)
