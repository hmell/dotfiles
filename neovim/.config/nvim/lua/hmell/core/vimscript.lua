vim.api.nvim_exec2([[
function! JumpToNextBufferInJumplist(dir) " 1=forward, -1=backward
  let jl = getjumplist() | let jumplist = jl[0] | let curjump = jl[1]
  let jumpcmdstr = a:dir > 0 ? '<C-O>' : '<C-I>'
  let jumpcmdchr = a:dir > 0 ? '' : '	'  " <C-I> or <C-O>
  let searchrange = a:dir > 0 ? range(curjump+1,len(jumplist))
                            \ : range(curjump-1,0,-1)
  for i in searchrange
      if jumplist[i]["bufnr"] != bufnr('%')
          let n = (i - curjump) * a:dir
          echo "Executing ".jumpcmdstr." ".n." times."
          execute "silent normal! ".n.jumpcmdchr
          break
      endif
  endfor
endfunction
nnoremap <leader><C-O> :call JumpToNextBufferInJumplist(-1)<CR>
nnoremap <leader><C-I> :call JumpToNextBufferInJumplist( 1)<CR>
]], {output = false})
