return {
  "goolord/alpha-nvim",
  event = "VimEnter",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  config = function()
    local alpha = require("alpha")
    local dashboard = require("alpha.themes.dashboard")

    -- Set header
    dashboard.section.header.val = {
      "                                                     ",
      "  ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗ ",
      "  ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║ ",
      "  ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║ ",
      "  ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║ ",
      "  ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║ ",
      "  ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝ ",
      "                                                     ",
    }


    -- Set menu
    dashboard.section.buttons.val = {
	  dashboard.button("f", "󰈞  Find file", ":FzfLua files <CR>"),
	  dashboard.button("e", "  New file", ":ene <BAR> startinsert <CR>"),
	  dashboard.button("b", "  Bookmarks", ":e ~/notes/notes/.bookmarks<CR>"),
	  dashboard.button("r", "󰋚  Recently used files", ":FzfLua oldfiles <CR>"),
	  dashboard.button("t", "󰊄  Find text", ":FzfLua live_grep <CR>"),
	  dashboard.button("c", "  Configuration", ":e ~/.config/nvim/after/plugin/init.lua <CR>"),
	  dashboard.button("q", "󰅖  Quit Neovim", ":qa<CR>"),
    }

    -- Send config to alpha
    alpha.setup(dashboard.opts)

    -- Disable folding on alpha buffer
    vim.cmd([[autocmd FileType alpha setlocal nofoldenable]])
  end,
}
