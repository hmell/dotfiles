return {
  "hmell/code-cells",
  dev = true,
  config = function ()

  local opts = {}

  local keymap = vim.keymap
  -- keymap.set('n', '<leader>xj', '<cmd>lua require("code-cells").move_next_cell()<CR>', opts)
  keymap.set('n', '<leader>xj', '<cmd>lua vim.fn.search("%#%")<CR>', opts)
  keymap.set('n', '<leader>xk', '<cmd>lua vim.fn.search("%#%", "b")<CR>', opts)
  keymap.set('n', '<leader>xc', 'i# %#%', opts)
  keymap.set('i', 'xcc', '# %#%', opts)
  -- keymap.set('n', '<leader>xk', '<cmd>lua require("code-cells").move_previous_cell()<CR>', opts)
  -- keymap.set('n', '<leader>xn', '<cmd>lua require("code-cells").move_next_cell("script")<CR>', opts)
  -- keymap.set('n', '<leader>xp', '<cmd>lua require("code-cells").move_previous_cell("script")<CR>', opts)
  -- keymap.set('n', '<leader>xx', '<cmd>lua require("code-cells").run_cell()<CR>', opts)
  -- keymap.set('n', '<leader>xs', '<cmd>lua require("code-cells").run_cell_script()<CR>', opts)
  -- keymap.set('n', '<leader>x<Enter>', '<cmd>lua require("code-cells").run_cell_move_next()<CR>', opts)
  -- keymap.set('n', '<leader>xm', '<cmd>lua require("code-cells").run_cell_move_next_script()<CR>', opts)
  -- keymap.set('n', '<leader><F6>', '<cmd>lua require("code-cells").run_all()<CR>', opts)
  keymap.set('n', '<leader><F5>', '<cmd>%SlimeSend<CR>', opts)

  end,
}

