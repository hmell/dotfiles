return {
  'ellisonleao/gruvbox.nvim',
  priority = 1000, -- make sure to load this before all other plugins
  config = function ()
    require("gruvbox").setup({
      undercurl = true,
      underline = true,
      bold = true,
      italic = { strings = true,
         operators = true,
         comments = true},
      strikethrough = true,
      invert_selection = false,
      invert_signs = false,
      invert_tabline = false,
      invert_intend_guides = false,
      inverse = true, -- invert background for search, diffs, statuslines and errors
      contrast = "soft", -- can be "hard", "soft" or empty string
      palette_overrides = {
          bright_aqua = "#87af5f",
          neutral_aqua = "#87af5f",
      },
      overrides = {
          markdownItalic = {fg = "#878787"},
          markdownBold = {fg = "#d0d0d0"},
          Normal = {fg = "#d7d7af"},
          IndentBlanklineChar = {fg = "#87af5f"}
      },
      dim_inactive = false,
      transparent_mode = false,
    })
    vim.o.background = "dark"
    vim.cmd("colorscheme gruvbox")
  end,
}
