return {
  'wilfreddenton/history.nvim',
  dependencies = { "nvim-lua/plenary.nvim" }
  ,
  config = function()
    local history = require('history')
    history.setup({
            keybinds = {
                back = '<C-P>',
                forward = '<C-N>',
                view = '<leader>bv'
            }
        })
  end
}
