return {
  "hmell/notes",
  dev = true,
  config = function ()

  local opts = {}

  local keymap = vim.keymap

  -- %#% Core Wiki commands
  keymap.set("n", "<leader>wb", '<cmd>lua require("notes").open_bookmarks()<CR>', opts) -- open bookmarks (`~/notes/notes/.bookmarks`)
  keymap.set("n", "<leader>wi", "<cmd> lua require('notes').list_links()<CR>", opts) -- list of linked notes
  keymap.set("n", "<leader>wp", "<cmd> lua require('notes').list_backlinks()<CR>", opts) -- list of linked notes
  keymap.set("n", "<leader>wf", '<cmd>lua require("notes").select_opened_notes()<CR>', opts) -- select opened notes based on titles
  keymap.set('n', '<leader>wl',  '<cmd>lua require("notes").infile_tags()<CR>', opts) -- infile tags
  keymap.set("n", "<leader>ww", '<cmd>lua require("notes").title_search(1)<CR>', opts) -- filter on title
  keymap.set("n", "<leader>wn", '<cmd>lua require("notes").tag_field_search(1)<CR>', opts)
  keymap.set("n", "<leader>wo", "<cmd> lua require('notes').open_note()<CR>", opts) -- open note under cursor
  keymap.set("n", "<leader>wh", "<cmd>lua require('notes').new_note()<CR>", opts)
  keymap.set("n", "<leader>wj", "<cmd>lua require('notes').new_note_and_link()<CR>", opts)
  keymap.set('n', '<leader>ws', '<cmd>lua require("tmux-functions").new_supp_note("new")<CR>', opts) -- open/create supp folder for current note
  keymap.set("n", "<leader>wI", "<cmd>lua require('notes').insert_image_link()<CR>", opts)
  keymap.set("n", "<leader>wu", "<cmd>lua require('notes').update_title()<CR>", opts)

  keymap.set("n", "<leader>w,", '<cmd>lua require("notes").goto_next_link()<CR>', opts)
  -- %#% Create Kukariri card
  keymap.set('n', '<leader>kk',  '<cmd>lua require("notes").new_card("default")<CR>', opts)
  keymap.set('n', '<leader>ka',  '<cmd>lua require("notes").new_card("audio")<CR>', opts)

  -- %#% Move last screenshot to ./img subfolder and paste markdown link below cursor 
  -- vim.cmd [[
  --   nnoremap <Leader>ic i![]()<Esc>:cd %:p:h <Bar> :execute '! $HOME/.scripts/img-paste.sh ' . shellescape(expand('%:t:r')) <CR><CR>:execute 'read ! echo $HOME/notes/supp/' . expand('%:t:r') . '/img/$(ls $HOME/notes/supp/' . expand("%:t:r") . '/img \|  tail -n 1)' <CR>0d$klllp0
  -- ]]

  -- %#% New log entry
  -- vim.api.nvim_create_user_command('LogEntry', 'lua require("notes").new_log_entry()<CR>',
  --   {desc = "Add a logbook entry below cursor"}) -- log entry

  -- %#% Compile/Open note in current buffer in different formats
  -- keymap.set('n', '<leader>qp', '<cmd>lua require("tmux-functions").quarto_preview("pdf", "compile")<CR>', opts)
  -- keymap.set('n', '<leader>qop', '<cmd>lua require("tmux-functions").quarto_preview("pdf", "open")<CR>', opts)
  -- keymap.set('n', '<leader>qh', '<cmd>lua require("tmux-functions").quarto_preview("html", "compile")<CR>', opts)
  -- keymap.set('n', '<leader>qop', '<cmd>lua require("tmux-functions").quarto_preview("html", "open")<CR>', opts)
  -- keymap.set('n', '<leader>qd', '<cmd>lua require("tmux-functions").quarto_preview("docx", "compile")<CR>', opts)
  -- keymap.set('n', '<leader>qop', '<cmd>lua require("tmux-functions").quarto_preview("docx", "open")<CR>', opts)
  -- keymap.set('n', '<leader>qs', '<cmd>lua require("tmux-functions").quarto_preview("revealjs", "compile")<CR>', opts)
  -- keymap.set('n', '<leader>qos', '<cmd>lua require("tmux-functions").quarto_preview("revealjs", "open")<CR>', opts)


  -- vim.cmd [[ 
  --   nnoremap <Leader>wh :silent ! $HOME/.scripts/nztl.sh<CR>:exe "e " . "$HOME/notes/notes/" . system("ls -ptr ~/notes/notes \| grep -v '/$' \| tail -1")<CR>jjA 
  -- ]] -- create and open new note

  -- vim.cmd [[
  --   nnoremap <Leader>wj a<Space>[]()<Esc>
  --     \ :exe "! $HOME/.scripts/nztl.sh ; ls $HOME/notes/notes/*.md \| tail -n 1 \| xargs grep id: \| head -n 1 \| cut -f2 -d' ' \| xclip"<CR>
  --     \ :exe "r !xclip -o"<CR>
  --     \ 0Dkk$P<Esc>
  --     \ :exe '! echo ' . shellescape(expand('%:t:r')) . " \| xclip"<CR>
  --     \ :exe 'e ' . system("ls ~/notes/notes/*.md \| tail -n 1")<CR>
  -- ]] -- create note and link to it

  --[[ Commands ]]

  vim.api.nvim_create_user_command('Date', 'lua require("notes").new_log_date()<CR>',
    {desc = "Add a 'log' date below cursor"}) -- log date

  -- %#% Command to open FZF menu with list of notes with YAML tags matching query 
  vim.api.nvim_create_user_command('QuickSearch',
    function(args)

      local query = vim.fn.shellescape(table.concat(args.fargs, " "))
      local fp = vim.fn.trim(vim.fn.system("rg --no-messages -l -e '" .. "^tags:.*" .. query .. "' $HOME/notes/notes | tr '\n' ' ' | sd '.$' ''"))

      opts = {}
      opts.prompt           = "> "
      opts.cwd              = "~/notes/notes"

      opts.fzf_opts = {
        ["--cycle"] = '',
        ["--with-nth"] = '2',
        ["--delimiter"] = '~',
        ["--preview"] = vim.fn.shellescape("echo {1} | xargs -I id bat --style=plain --color=always id"),
      }

      opts.winopts = {
        preview = {
          layout = "vertical",
          vertical = "up:70%", wrap = "wrap"
        },
        width = 0.8,
      }

       opts.actions = {
           ['default'] = function(selected)
             local path = string.match(selected[1], '%d+')
             vim.cmd('e ' .. '$HOME/notes/notes/' .. path .. '.md')
          end,
           ['alt-q'] = function()
             vim.cmd('qa!')
          end
       }
      return require'fzf-lua'.fzf_exec('rg "^title:" ' .. fp .. ' | sed -e "s/:title:/ ~/"', opts)
    end,
    {desc = "FZF menu to filter a list of notes based on tag query"})
  end,
}
