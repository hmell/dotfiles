return {
  "hrsh7th/nvim-cmp",
  event = "InsertEnter",
  dependencies = {
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-nvim-lsp-signature-help",
    "hrsh7th/cmp-calc",
    "hrsh7th/cmp-emoji",
    "f3fora/cmp-spell",
    "hrsh7th/cmp-buffer", -- source for text in buffer
    "hrsh7th/cmp-path", -- source for file system paths
    "L3MON4D3/LuaSnip", -- snippet engine
    "saadparwaiz1/cmp_luasnip", -- for autocompletion
    "rafamadriz/friendly-snippets", -- useful snippets
    "onsails/lspkind.nvim", -- vs-code like pictograms
    "kdheepak/cmp-latex-symbols",
    "jmbuhr/cmp-pandoc-references",
    "ray-x/cmp-treesitter",
  },
  config = function()
    local cmp = require("cmp")
    local ls = require("luasnip")
    local lspkind = require("lspkind")

    -- loads vscode style snippets from installed plugins (e.g. friendly-snippets)
    require("luasnip.loaders.from_vscode").lazy_load()
    require("luasnip.loaders.from_vscode").lazy_load({ paths = "~/.config/nvim/snippets/vscode-snips" })
    require("luasnip.loaders.from_lua").load({paths = "~/.config/nvim/snippets/lua-snips"})

    -- keymaps snippets in insert mode
    vim.keymap.set({"i", "s"}, "<c-d>", function()
            if ls.expand_or_jumpable() then
                ls.expand_or_jump()
            end
        end, {silent = true})
    vim.keymap.set({"i", "s"}, "<c-s>", function()
            if ls.jumpable(-1) then
                ls.jump(-1)
            end
        end, {silent = true})
    vim.keymap.set({"i", "s"}, "<c-o>", function()
    	if ls.choice_active() then
    	    ls.change_choice(1)
    	end
    end, {silent = true})

    cmp.setup({
      completion = {
        completeopt = "menu,menuone,preview,noselect",
      },
      snippet = { -- configure how nvim-cmp interacts with snippet engine
        expand = function(args)
          ls.lsp_expand(args.body)
        end,
      },
      mapping = cmp.mapping.preset.insert({
        ["<C-b>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(), -- show completion suggestions
        ["<C-e>"] = cmp.mapping.abort(), -- close completion window
        ["<CR>"] = cmp.mapping.confirm({ select = false }),
      }),
      -- sources for autocompletion
      sources = cmp.config.sources({
        { name = 'otter' }, -- for code chunks in quarto
        { name = "luasnip" }, -- snippets
        { name = 'path' },
        { name = "nvim_lsp" },
        { name = 'nvim_lsp_signature_help' },
        { name = 'pandoc_references' },
        { name = 'buffer', keyword_length = 3, max_item_count = 3 },
        { name = 'spell' },
        { name = 'treesitter', keyword_length = 3, max_item_count = 3 },
        { name = 'calc' },
        { name = 'latex_symbols' },
        { name = 'emoji' },
      }),
      -- configure lspkind for vs-code like pictograms in completion menu
      formatting = {
        format = lspkind.cmp_format({
          with_text = true,
          menu = {
            otter = "[🦦]",
            luasnip = "[snip]",
            nvim_lsp = "[LSP]",
            buffer = "[buf]",
            path = "[path]",
            spell = "[spell]",
            pandoc_references = "[ref]",
            tags = "[tag]",
            treesitter = "[TS]",
            calc = "[calc]",
            latex_symbols = "[tex]",
            emoji = "[emoji]",
          },
          maxwidth = 50,
          ellipsis_char = "...",
        })
      },
    })

    -- link quarto and rmarkdown to markdown snippets
    ls.filetype_extend("quarto", { "markdown" })
    ls.filetype_extend("rmarkdown", { "markdown" })

  end,
}
