return {
  'quarto-dev/quarto-nvim',
  dev = false,
  dependencies = {
    {
      'jmbuhr/otter.nvim',
      dev = false,
      dependencies = {
        { 'neovim/nvim-lspconfig' },
      }, opts = {}
      -- opts = {
      --   lsp = {
      --     hover = {
      --       border = require 'misc.style'.border
      --     }
      --   }
      -- }
    },
  },
  config = function()
    local quarto = require('quarto')
    quarto.setup({
      debug = false,
      closePreviewOnExit = true,
      lspFeatures = {
        enabled = true,
        languages = { 'r', 'python', 'bash' },
        diagnostics = {
        enabled = true,
        triggers = { "BufWrite" }
        },
        completion = {
          enabled = true,
        },
      },

    })
  end
}
