return {
  "hmell/tmux-functions",
  dev = true,
  config = function ()

  local opts = {}

  local keymap = vim.keymap
  keymap.set("n", "<leader>lf", "<cmd>lua require('tmux-functions').open_lf()<CR>", opts)
  keymap.set("n", "<leader>sc", "<cmd>lua require('tmux-functions').nap('copy')<CR>", opts)
  keymap.set("n", "<leader>se", "<cmd>lua require('tmux-functions').nap()<CR>", opts)
  keymap.set("n", "<leader>sp", "<cmd>lua require('tmux-functions').nap('read')<CR>", opts)
  end,
}

