return {
  -- Klafyvel/vim-slime-cells
  'hugomell/vim-slime-cells',
  config = function()
    vim.keymap.set('n', "<leader>cc", "<Plug>SlimeCellsSendAndGoToNext", { silent = true, noremap = true })
    vim.keymap.set('n', "<leader>cj", "<Plug>SlimeCellsNext", { silent = true, noremap = true })
    vim.keymap.set('n', "<leader>ck", "<Plug>SlimeCellsPrev", { silent = true, noremap = true })
  end
}
