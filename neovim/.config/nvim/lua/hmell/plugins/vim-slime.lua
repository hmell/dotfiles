return {
  'jpalardy/vim-slime',
  config = function()
    vim.g.slime_target = "tmux"
    vim.g.slime_cell_delimiter = ""
    -- vim.cmd [[ let g:slime_default_config = {"socket_name": "default", "target_pane": "{down-of}"} ]]
    vim.g.slime_default_config = { socket_name = "default", target_pane = "{right-of}" }
    vim.g.slime_python_ipython = 1
    vim.g.slime_bracketed_paste = 1
    vim.g.slime_no_mappings = 1
    -- vim.keymap.set('n', "<leader><CR>", "<Plug>SlimeSendCell", { silent = true, noremap = true })
  end
}

