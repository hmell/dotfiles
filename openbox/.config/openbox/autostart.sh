# nitrogen --restore &
feh --randomize --bg-center pictures/wallpapers
sxhkd &
copyq &
/usr/lib/notification-daemon/notification-daemon &
setxkbmap -option caps:ctrl_modifier
