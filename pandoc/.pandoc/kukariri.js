<script>
// Display answer on click and play audio if relevant
document.addEventListener("DOMContentLoaded", function() {
  // Get references to the button and the element to toggle
  const toggleButton = document.getElementById("toggleButton");
  const backCard = document.getElementById("back");
  const frontCard = document.getElementById("front");
  const audio = document.getElementById("audioSample");
  function playAudio() {
      audio.play()
  }

  // Add a click event listener to the button
  toggleButton.addEventListener("click", function() {
    frontCard.style.display = "none";
    backCard.style.display = "block";
    playAudio()
    });
  });
</script>
