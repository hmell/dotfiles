#!/bin/bash

# Target notes
read -d '' targets << EOF
[bookmarks--ipea]:20230620093530.md
[cheatsheets]:20230727163933.md
[learning-inbox]:20230430114454.md
[learning-queue]:20230731092947.md
[learning-log]:.md
[library-courses]:20230731095631.md
[library-other]:20230731101018.md
[library-papers]:20230602112902.md
[library-textbooks]:20230731090557.md
[movies-inbox]:20210731163440.md
[music-inbox]:20220410164716.md
[planning]:20230723100047.md
[reading-inbox]:20230502181506.md
[scratchpad]:20231005230457.md
[todo-list]:20230107152401.md
EOF

# Choose file to open
choice=$(echo "$targets" | sort| fzf -m --cycle --prompt "Quick notes>" | cut -f2 -d':')

[[ -z "$choice" ]] && exit

tmux neww -c $HOME/notes/notes/ -n quick-notes nvim $choice
