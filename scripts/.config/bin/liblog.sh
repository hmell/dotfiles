#!/bin/bash

# Features:
# - [✓] menu to select type of log I want to look at (e.g. comics, ...)
# - [✓] display corresponding log entries
# - filter log entries by tags


# Menu with different library logs
log_type="$(
(cat << EOF
audiobooks
movies
shows
comics
fictions
essays
science
textbooks
EOF
) | fzf --reverse)"


get_log_entries ()
{
  fd -t f -a --full-path "${log_type}"  $HOME/library/_log/ \
    | sort -t'/' -k8 \
    | fzfub -e --reverse --no-sort --cycle
  
}

get_log_entries
