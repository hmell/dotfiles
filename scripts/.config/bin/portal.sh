#!/bin/bash

# Target files
read -d '' targets << EOF
[this-is-the-way]:notes/notes/20240105170824.md
[task-list]:notes/notes/20230723100047.md
[todo-list]:notes/notes/20230107152401.md
[keybindings]:notes/notes/20230606085613.md
[cheatsheets]:notes/notes/20230727163933.md
[snippets]:notes/notes/20240105191603.md
[scratchpad-latex]:code/_sandbox/scratchpad.tex
[scratchpad-python]:code/_sandbox/scratchpad.py
[scratchpad-R]:code/_sandbox/scratchpad.R
[scratchpad-bash]:code/_sandbox/scratchpad.bash
[scratchpad-markdown]:notes/notes/20231005230457.md
[lf]:.config/lf/lfrc
[mimeapps]:.local/share/applications/mimeapps.list
[quick-docs]:.config/bin/qdocs.sh
[tmux]:.tmux.conf
[zsh]:.zshrc
[zsh-shell-aliases]:.config/zsh/aliases.zsh
[zsh-shell-functions]:.config/zsh/functions.zsh
[portal]:.config/bin/portal.sh
EOF

# Choose file to open
choice=$(echo "$targets" | sort| $HOME/.fzf/bin/fzf -m --cycle --prompt "Portal>" | cut -f2 -d':')

[[ -z "$choice" ]] && exit

tmux neww -c $HOME/ -n portal nvim $choice
# tmux switch-client -t home:portal
