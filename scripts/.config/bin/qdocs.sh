#!/bin/bash

selection=$(printf "textbooks\nshortbooks\npapers\ncheatsheets\n" | /home/hugo/.fzf/bin/fzf --tac)

if [[ -z "$selection" ]]; then
  exit
fi

if [[ $(printf "$selection") == "cheatsheets" ]]; then
  zathura --fork $(ls $HOME/library/_files/cheatsheets/*.pdf | /home/hugo/.fzf/bin/fzf -m --cycle)
elif [[ $(printf "$selection") == "papers" ]]; then
  zathura --fork $(ls $HOME/library/_files/papers/*.pdf | /home/hugo/.fzf/bin/fzf -m --cycle)
elif [[ $(printf "$selection") == "shortbooks" ]]; then
  zathura --fork $(ls $HOME/library/_files/shortbooks/*.pdf | /home/hugo/.fzf/bin/fzf -m --cycle)
else
  zathura --fork $(ls $HOME/library/_files/textbooks/*.pdf | /home/hugo/.fzf/bin/fzf -m --cycle)
fi
