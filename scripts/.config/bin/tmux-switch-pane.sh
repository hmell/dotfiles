#!/bin/bash

# customizable
# LIST_DATA="\u001b[36;1m#{session_name}\u001b[0m - #{window_name} #{pane_title} #{pane_current_path} #{pane_current_command}"
LIST_DATA="#{session_name} - #{window_name} - #{pane_current_path} #{pane_current_command}"
# FZF_COMMAND="fzf-tmux -p --ansi --delimiter=: --with-nth 4 --color=hl:2"
CURRENT_SESSION="$(tmux display-message -p '#S')"

# FZF_COMMAND="fzf-tmux -p -- --delimiter=: --with-nth 4 --ansi --tiebreak=begin --query $CURRENT_SESSION"
FZF_COMMAND="$HOME/.fzf/bin/fzf-tmux -p -- --delimiter=: --with-nth 4 --ansi --tiebreak=begin"

# do not change
TARGET_SPEC="#{session_name}:#{window_id}:#{pane_id}:"


# select pane
LINE=$(sed 's,^\([^ ]*\) \([^ ]*\) - \([a-zA-Z-]*\) - \([^ ]*\) \(.*\),\1 \x1B[36m\2\x1B[0m - \x1B[32m\3\x1B[0m - \4 \x1B[31m\5\x1B[0m,' <(tmux list-panes -a -F "$TARGET_SPEC $LIST_DATA") | $FZF_COMMAND) || exit 0

# split the result
args=(${LINE//:/ })
# activate session/window/pane
tmux select-pane -t ${args[2]} && tmux select-window -t ${args[1]} && tmux switch-client -t ${args[0]}
