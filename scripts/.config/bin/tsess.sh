#!/bin/bash

sessions=$HOME/.tmux/sessions

target=$(ls $sessions | dmenu -l 10 -i)
[[ -z "$target" ]] && exit

tmux new-window -S -c $HOME -n sessions-script -t home nvim "${sessions}/${target}"
tmux switch -t home
