#!/bin/bash

DIR=$(realpath $1)
ALBUM_TITLE=$(basename $(realpath $1) | sed 's/_/ /g')

# Chech called with resizing parameters
if [ -z "$2" ] ; then echo "Please provide resizing height and width" ; exit ; fi

# Full one liner for generating tracklist image
TRACKLIST=$(printf "$(find $DIR -not -path '*/\.*' -iname '*.flac' -print0 -o \
  -not -path '*/\.*' -iname '*.mp3' -print0 -o \
  -not -path '*/\.*' -iname '*.m4a' -print0 | \
  xargs -I{} -0 ~/.scripts/audiofileDuration.sh {} | \
  sed -E -e 's/\.mp3|\.flac|\.m4a//;s/_/ /g;s/ /\. /;s:^./::' | sort | \
  sed '0~1 a\\')")

convert -background black -fill white -font "FreeMono" -pointsize 32 \
  -size "$2"x"$3" -gravity center -interline-spacing -3 \
  label:"$(printf '%s\n\n\n%s\n' "$ALBUM_TITLE" "$TRACKLIST")" $DIR/tracklist.jpg

