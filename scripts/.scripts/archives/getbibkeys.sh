#!/bin/bash
bib_file="$HOME/library/references.bib"

if [ "$1" == "-f" ] ; then
  bibtex-ls "${bib_file}" | fzf --multi --ansi | bibtex-cite | sed 's/@//g;s/; /\\|/g'
else
  if [ -z "$1" ] ; then echo "Please provide an input file or use -f for fuzzy matching with fzf" ; exit ; fi
  rg -N -o "[\s\[\"'(\{]@[[:alnum:]]+?[\s\]\"')},;]" "$1" | sort | uniq | \
    sed 's/^.\(.*\).$/\1/;s/@//' | tr '\n' '|' | sed 's/|$//;s/|/\\|/g'
fi

