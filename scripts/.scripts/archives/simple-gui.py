#!/usr/bin/env python3

import PySimpleGUI as sg

sg.theme('DarkAmber')	# Add a touch of color
# All the stuff inside your window.
layout = [ [sg.Text('Track Numbers'), sg.InputText()],
           [sg.Submit(), sg.Cancel()] ]

# Create the Window
window = sg.Window('Select Tracks', layout, location=(0,0))

event, values = window.read()

window.close()

print(values[0])     # the first input element is values[0]
