#!/bin/bash

#tmux new-session -s TEST -n asciiquarium -d
#tmux new-window -t TEST -d -n weather
#
#tmux send-keys -t TEST:asciiquarium "asciiquarium" Enter
#tmux send-keys -t TEST:weather "curl wttr.in | head -n -2" Enter
#
#tmux select-window -t TEST:asciiquarium
#tmux split-window -h
#tmux send-keys -t TEST:asciiquarium "asciiquarium" Enter
#
#urxvt -e tmux -u attach -t TEST
