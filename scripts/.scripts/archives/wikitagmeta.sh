#!/bin/bash
WIKI=$HOME/Notes/wiki
matches=$(rg --files-with-matches "${1}" $WIKI)
rg -o -N --no-filename --no-heading -e "'#\w.*'" $matches |
#sed -r "s:[\s'#]::g" |
sed -r "s:,\s:\n:g" |
sort |
uniq
