#!/bin/bash
ZOTLIB="$HOME/Library/zotero-lib/"
QUERY="$1"'.*'"$2"'.*.pdf'
MATCHES=$(ls ~/Library/zotero-lib | grep -E "$QUERY")
COUNT=$(printf "$MATCHES\n" | wc -l)

if [ $COUNT != 1 ]; then
    MATCH=$(select item in $MATCHES; do echo $item; break; done)
    zathura "${ZOTLIB}${MATCH}"
else
    MATCH=$MATCHES
    zathura "${ZOTLIB}${MATCH}"
fi
