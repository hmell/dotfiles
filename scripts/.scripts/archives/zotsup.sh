#!/bin/bash
PDF=$1
FILES="${@:2}"
FOLDER_NAME=$(echo ${PDF%.pdf} | cut -f 6 -d /)
FOLDER="$HOME/Library/zotero-lib/attachments/${FOLDER_NAME}"

mkdir $FOLDER
mv $FILES $FOLDER
