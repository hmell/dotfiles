#!/bin/bash
printf "$(basename $1)  ($(ffmpeg -i "$1" 2>&1 | grep Duration | grep -o -P '[0-9]{2}:.*?[0-9]\.' | sed 's/00://' | tr -d . )) \n"
