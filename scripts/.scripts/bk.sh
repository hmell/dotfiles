#!/bin/bash

set -e



add_tags ()
{
  # args: <file with documents info>
  current_tags=$(rg -NI '^tags: _\|#.*\|_' $1 | perl -ne 'print "$1\n" while /(#.*?)\|/g;' | sort -u)

  status="inprogress"
  tag_list=""

  while [ "$status" ]; do
    choice=$(echo "$current_tags" | /home/hugo/.fzf/bin/fzf -m --bind alt-enter:print-query | tr '\n' ' ')
      if [[ -z "$choice" ]]; then
        status=""
      else
        tag_list="$tag_list$choice"
      fi
  done

  echo "$tag_list"
}



documents_tags ()
{
  # args: <file with documents info>
  rg -NI '^tags: _\|#.*\|_' "$1" \
    | perl -ne 'print "$1\n" while /(#.*?)\|/g;' \
    | sort -u \
    | awk '{ printf "~~~~~~~~<placeholder>~~~~~~~~" $0 "\n" }'
}



generate_git_grep_pattern ()
{
  # args: [<tag1>, <tag2>, <tag3>, ...]
  tags="$@"
  first_tag="$(echo $tags | cut -d ' ' -f1)"
  other_tags="$(echo $tags | cut -d ' ' -f2-)"
  pattern="-e '${first_tag}'"

  for v in $(echo $other_tags) ; do
    pattern="$pattern --and -e '$v'" 
  done

  echo "$pattern"
}



get_document_info ()
{
  # args: <document id>, <file with documents info>, <document type>
  # Append only lines for given bookmark id
  sed  "/id: ${1}/,/^label: /!d;/^label: /q" $2 >> "/tmp/${3}-filtered.md"
  printf '\n\n\n' >> "/tmp/${3}-filtered.md"
}



filter_documents ()
{
  # args: <file with documents info>, <document type>,
  #       [<tag1>, <tag2>, <tag3>, ...]

  # if [[ "$1" == "$HOME/notes/.bookmarks.md" ]]; then
  #     fp=".bookmarks.md"
  # else
  #     fp=".bookmarks_archive.md"
  # fi
  fp="$(basename $1)"
  doctype="$2" 
  shift 2
  nb_tags="$(echo $@ | tr ' ' '\n' | wc -l)"
  if [[ "$nb_tags" -eq 1 ]]; then
      pattern="-e '$1'"
  else
      pattern=$(generate_git_grep_pattern "$@")
  fi

  docs_ids="$(eval git --no-pager grep -h -B 1 $pattern --no-index $fp | rg '^id: ' | cut -d' ' -f2 | tr '\n' ' ' | sed 's/.$//')"

  # temporary empty file to receive filtered documents
  :> /tmp/${doctype}-filtered.md

  # Append info for each matching document
  for i in $docs_ids ; do
      get_document_info  $i $fp $doctype
  done

  # Generate new input file for fzf with only matching documents
  fzf_document_info /tmp/${doctype}-filtered.md $doctype

}



add_paper ()
{
  # args: NA
  paper_id=$(date +%Y%m%d%H%M%S)
  doi="$(copyq clipboard)"

  # get metadata from crossref API
  # https://api.crossref.org/works/<doi>/
  metadata=$(curl -s https://api.crossref.org/works/${doi}/ \
    | jaq '.message')

  # Get info from json object
  title=$(echo "$metadata" | jaq '.title[0]' | tr -d '"')
  year=$(echo "$metadata" | jaq '.published["date-parts"][0][0]' | tr -d '"')
  journal=$(echo "$metadata" | jaq '.["container-title"][0]' | tr -d '"')
  authors=$(echo "$metadata" \
    | jaq '.author | map([.given, .family] | "\(.[0]) \(.[1])")' \
    | tr -d '"' | cat <(echo 'authors:') -)

  # Extract first author and create label
  first_author=$(echo "$authors" | head -n 3 | tail -n1 | tr -d ',"' | sed 's/^\s*//' | rev | cut -d' ' -f1 | rev)
  label="$first_author $year - $title"

  # add tags for paper
  lastname=$(echo $first_author | tr '[:upper:]' '[:lower:]')
  key_tag="#key::${lastname}${year}"
  tags="_|$(add_tags $1 | tr ' ' '|')${key_tag}|_"

  # Choose url or filepath to add
  gum format -- "## Choose type of link to add:"
  choice=$(gum choose "url" "filepath")

  if [[ "$choice" == "url" ]]; then
    link="$(copyq clipboard)"
  else
    link="$HOME/library/_files/papers/$(ls -ltr $HOME/library/_files/papers/ | rev | cut -d' ' -f1 | rev | /home/hugo/.fzf/bin/fzf --no-sort)"
  fi

  # format output
  cat << EOF >> $1



id: $paper_id
tags: $tags
title: $title
year: $year
journal: $journal
$authors
doi: $doi
url: $link
label: $label
EOF
    
}

add_bibtex_paper ()
{
  doi="$1"
  bibtex=$(curl -LH https://doi.org/${doi}/)
}



add_textbook ()
{
  
  # args: NA
  textbook_id=$(date +%Y%m%d%H%M%S)
  isbn="$(copyq clipboard)"

  # get metadata from Open Library API
  # https://openlibrary.org/developers/api

  # surrounding double quotes are needing for jaq
  key='"ISBN:'"$isbn"'"'
  metadata=$(curl -s "https://openlibrary.org/api/books?bibkeys=ISBN:${isbn}&jscmd=details&format=json" \
    | jaq ".${key}.details")

  title=$(echo "$metadata" | jaq '.title')
  year=$(echo "$metadata" | jaq '.publish_date' | cut -f2 -d"," | tr -d ' "')

  authors=$(echo "$metadata" \
    | jaq '.authors | map([.name] | .[0])' \
    | tr -d '"' | cat <(echo 'authors:') -)

  first_author=$(echo "$authors" | head -n 3 | tail -n1 | tr -d ',"' | sed 's/^\s*//' | rev | cut -d' ' -f1 | rev)
  label=$(echo "$first_author $year - $title" | tr -d '"')

  # add tags for textbook
  lastname=$(echo $first_author | tr '[:upper:]' '[:lower:]')
  key_tag="#key::${lastname}${year}"
  tags="_|$(add_tags $1 | tr ' ' '|')${key_tag}|_"

  # Choose url or filepath to add
  gum format -- "## Choose type of link to add:"
  choice=$(gum choose "url" "filepath")

  if [[ "$choice" == "url" ]]; then
    link="$(copyq clipboard)"
  else
    link="$HOME/library/_files/textbooks/$(ls -ltr $HOME/library/_files/textbooks/ | rev | cut -d' ' -f1 | rev | /home/hugo/.fzf/bin/fzf --no-sort)"
  fi

  # format output
  cat << EOF >> $1



id: $textbook_id
tags: $tags
title: $title
year: $year
$authors
isbn: $isbn
url: $link
label: $label
EOF
    
}


add_bookmark ()
{
  # args: NA
  id=$(date +%Y%m%d%H%M%S)
  url="$(copyq clipboard)"
  html="$(curl -s $url | sd '.*<title' '<title')"
  title=$(echo "$html" | rg -or '$1' '>(.*)</title>')

  title=$(gum input --prompt="Title> " --value="$title")
  label=$(gum input --prompt="Label> " --value="$title")
  description="$(gum write --placeholder 'Write a short description (CTRL+D to finish)' | fold -s)"

  tags="_|$(add_tags $1 | tr ' ' '|')_"

  cat << EOF >> $1



id: $id
tags: $tags
title: $title
description:
$description
url: $url
label: $label
EOF

}



preview_document ()
{
  # args: <file with documents info>, <document id>

  awk -v target="$2" '
$0 ~ "^id: " target {
  flag=1
  next
}
flag && /^title: |^tags: / {
  printf $0 "\n\n"
  next
}
flag && /^url: / {
  printf "\n\n" $0
  flag=0
  next
}
flag {
  print $0
}
{
}
' "$1"
}



open_document ()
{
  # args: <document type> <link> 
  dtach -n /tmp/docpreview-dtach xdg-open "$2" > /dev/null 2>&1 &
  sleep 1s
  rm /tmp/docpreview-dtach
}



edit_document ()
{
  # args: <file with documents info>, <document id>
  # open bookmark with cursor on matching id
  nvim "+/id: $2" $1
}



add_to_archive ()
{
  # args: <document id>, <file with documents info>, <archive file>
  awk -v target="$1" '
$0 ~ "^id: " target {
  flag=1
  print $0
  next
}
flag && /^label: / {
  flag=0
  print $0 "\n"
  next
}
flag {
  print $0
}
{
}
' "$2" >> "$3"
    
}



remove_from_documents ()
{
  # args: <document id>, <file with documents info>, <document type>
   
  # copy documents info to temporary file
  cp "$2" "/tmp/$3.md"

  # remove target bookmark
  awk -v target="$1" '
/^<!--/ || $0 ~ "^id: " target {
  flag=1
  print $0
  next
}
flag && /^label: / {
  flag=0
  print $0 "\n"
  next
}
flag && /^-->$/ {
  flag=0
  print $0 "\n\n\n"
  next
}
flag {
  print $0
}
{
}
' "/tmp/$3.md" > "$2"

  rm "/tmp/$3.md"
}



archive_document ()
{
  # args: <document id>, <file with documents info>, <document type>
  add_to_archive "$1" "$2" "$HOME/notes/.${3}_archive.md"
  remove_from_documents "$1" "$2" "$3"
}




fzf_document_info ()
{
  # args: <file with documents info>, <document type>

  cat $1 | awk '
/^id: |^url: /{
  printf substr($0, index($0,$2)) "~~~~~~~~"
  next
}
/^label: /{
  printf substr($0, index($0,$2)) "\n"
}
{
}
' > "/tmp/$2-fzf.md"
}



print_label_and_url ()
{
  sed  "/id: ${1}/,/^label: /!d;/^label: /q" $2 > "/tmp/${3}-linked.md"
  info=$(tac "/tmp/${3}-linked.md" | awk '
    /label/ {$1=""; printf "\*" $0 "\n" }
    /url/ { printf "  [url](" $2 ")\n"}
    ')
  copyq add "$info"
}



document_menu ()
{
  # args: <file with documents info>, <document type>
  fzf_document_info "$1" "$2"
  echo ${2}
  cat "/tmp/${2}-fzf.md" | /home/hugo/.fzf/bin/fzf -m --cycle --preview-window=65%:wrap --delimiter='~{8}' --with-nth=3 \
    --preview="/home/hugo/bin/bk --preview $1 {1}" \
    --bind "ctrl-o:execute@/home/hugo/bin/bk --open $2 {2}@" \
    --bind "alt-t:execute(/home/hugo/bin/bk --tags $1 | sed 's/<placeholder>//g' | tr -d '~' | tr ' ' '\n' | gum filter --no-limit | /home/hugo/bin/bk --filter $1 $2)+reload(cat /tmp/${2}-fzf.md)" \
    --bind "alt-r:execute@/home/hugo/bin/bk --fzf $1 $2@+reload(cat /tmp/$2-fzf.md)+show-preview" \
    --bind "alt-y:execute-silent(echo {3} | xargs -I tags copyq copy tags)" \
    --bind "alt-f:execute@/home/hugo/bin/bk --link {1} $1 $2@+reload(cat /tmp/$2-fzf.md)+show-preview" \
    --bind "ctrl-e:execute@/home/hugo/bin/bk --edit $1 {1}@"
    # --bind "alt-d:execute@/home/hugo/bin/bk --delete {1} $1 $2@+reload(/home/hugo/bin/bk --fzf $1 $2)" \
    # --bind "ctrl-x:execute@/home/hugo/bin/bk --archive {1} $1 $2@+execute@/home/hugo/bin/bk --fzf $1 $2@+reload(cat /tmp/bookmarks-fzf.md)" \
}





#------------------------------------------------------------------------------
#  Main program
#------------------------------------------------------------------------------

main () {

cd $HOME/notes
case "$1" in

  -t|--tags)
    shift
    # args: <file with documents info>
    documents_tags "$1"
    ;;

  -l|--link)
    shift
    # args: <document id> <file with documents info>, <document type>
    print_label_and_url "$1" "$2" "$3"
    ;;

  -o|--open)
    shift
    # args: <document type> <link> 
    open_document "$1" "$2"
    ;;

  -d|--delete)
    # args: <document id>, <file with documents info>, <document type>
    shift
    remove_from_documents "$1" "$2" "$3"
    ;;

  -X|--archived)
    shift
    cd $HOME/notes
    # args: <file with documents info>, <document type>
    document_menu "$HOME/notes/.${1}_archive.md" $1
    ;;

  -x|--archive)
    shift
    # args: <document id>, <file with documents info>, <document type>
    archive_document "$1" "$2" "$3"
    ;;

  -a|--add)
    case "$2" in
        url) add_bookmark "$HOME/notes/.bookmarks.md"
        ;;
        doi) add_paper "$HOME/notes/.bookmarks.md"
        ;;
        isbn) add_textbook "$HOME/notes/.bookmarks.md"
        ;;
        *) echo "Please provide the type of identifier used."
        ;;
    esac
    ;;

  -e|--edit)
    shift
    # args: <file with documents info>, <document id>
    edit_document "$1" "$2"
    ;;

  -f|--fzf)
    shift
    # args: <file with documents info>, <document type>
    fzf_document_info "$1" "$2"
    ;;

  -F|--filter)
    shift

    tags=$(while read -r t; do
      printf '%s ' $t
    done | sed 's/\s$//')

    # args: <file with documents info>, <document type>,
    filter_documents $1 $2 $tags
    ;;

  -p|--preview)
    shift
    # args: <file with documents info> <document id>
    preview_document "$1" "$2"
    ;;

  *)
    document_menu $HOME/notes/.bookmarks.md bookmarks
    ;;
esac

}

main "$@"
