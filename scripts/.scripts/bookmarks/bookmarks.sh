#!/bin/bash

selection=$(cat <(printf "_ADD BOOKMARK_\n_EDIT BOOKMARKS_\n") ~/.bookmarks | fzf --delimiter="~" --with-nth=1 --tac --preview='printf "%s\n\n%s" {2} {3}' --preview-window='wrap:down')

if [[ -z "$selection" ]]; then
  exit
fi

if [[ $(printf "$selection") == "_ADD BOOKMARK_" ]]; then
  nvim -c 'norm G' -c 'put "*' -c 'norm I[]~F]' +'star' ~/.bookmarks
elif [[ $(printf "$selection") == "_EDIT BOOKMARKS_" ]]; then
  nvim ~/.bookmarks
else
  echo "$selection" | cut -d '~' -f3 | $HOME/.cargo/bin/sd '^\~' '' | xclip -sel clip
  firefox --new-window "$(xclip -o -sel clip)"
  exit 0
fi
