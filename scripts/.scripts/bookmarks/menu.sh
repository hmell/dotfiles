#!/bin/bash

grep "bookmarks::" ~/.bookmarks | sed "s/bookmarks:://" | sort \
| column -t -l 2 -s "~" \
| dmenu -p "Bookmarks> " -l 30 \
| cut -f2 -d"~" | xargs -r firefox --new-window
