#!/bin/bash

set -e

root_dir="$HOME/code/git/"

cd $root_dir


repos=$(find . -name .git -type d -prune -exec dirname {} \; | tr "\n" " ")

for repo in $(echo $repos) ; do
    cd $repo
    label=$(basename $repo)
    git bundle create "$label.bdl"  --all && \
    mv "$label.bdl" "$root_dir/.bundles" || \
    echo "Problem while creating bundle"
    cd $root_dir
done

echo "Git bundling done! \o/"
