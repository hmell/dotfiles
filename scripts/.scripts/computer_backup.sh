#!/bin/bash

printf "LOG computer backup - $1 --- $(date)\n" > $HOME/.backup_log.txt 

backup_hidden_dirs ()
{
  ## khal data (calcurse replacement)
  rclone sync "$1" -P ~/.calendars pcloud:linux-backup/calendars | tee -a $HOME/.backup_log.txt

  ## lazy.nvim backup plugins versions
  rclone sync "$1" -P ~/.lazy-nvim-backup pcloud:linux-backup/.lazy-nvim-backup | tee -a $HOME/.backup_log.txt
}

backup_code_repos ()
{
  ## generate bundles for the git repositories in code/
  $HOME/.scripts/bundle_repos.sh

  rclone sync "$1" --exclude ".git/" --exclude "renv/library/**" --exclude ".devbox/" \
      -P ~/code pcloud:linux-backup/code | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" --exclude ".git/" --exclude "tmux/.tmux/plugins/" \
      -P ~/dotfiles pcloud:linux-backup/dotfiles | tee -a $HOME/.backup_log.txt 
}

backup_docs_and_work ()
{
  rclone sync "$1" -P ~/documents pcloud:linux-backup/documents | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P ~/work --exclude ".git/**" --exclude "renv/library/**" --exclude ".devbox/" pcloud:linux-backup/work | tee -a $HOME/.backup_log.txt 
}

backup_library ()
{
  if [[ "$1" == "workstation" ]]; then
    # backup only files useful for work
    rclone sync "$2" -P ~/library/_files/_covers/textbooks pcloud:linux-backup/library/_files/_covers/textbooks | tee -a $HOME/.backup_log.txt 
    rclone sync "$2" -P ~/library/_files/cheatsheets pcloud:linux-backup/library/_files/cheatsheets | tee -a $HOME/.backup_log.txt 
    rclone sync "$2" -P ~/library/_files/courses pcloud:linux-backup/library/_files/courses | tee -a $HOME/.backup_log.txt 
    rclone sync "$2" -P ~/library/_files/misc-docs pcloud:linux-backup/library/_files/misc-docs | tee -a $HOME/.backup_log.txt 
    rclone sync "$2" -P ~/library/_files/papers pcloud:linux-backup/library/_files/papers | tee -a $HOME/.backup_log.txt 
    rclone sync "$2" -P ~/library/_files/roadmaps pcloud:linux-backup/library/_files/roadmaps | tee -a $HOME/.backup_log.txt 
    rclone sync "$2" -P ~/library/_files/shortbooks pcloud:linux-backup/library/_files/shortbooks | tee -a $HOME/.backup_log.txt 
    rclone sync "$2" -P ~/library/_files/talks pcloud:linux-backup/library/_files/talks | tee -a $HOME/.backup_log.txt 
    rclone sync "$2" -P ~/library/_files/textbooks pcloud:linux-backup/library/_files/textbooks | tee -a $HOME/.backup_log.txt 
    rclone sync "$2" -P ~/library/_files/references.bib pcloud:linux-backup/library/_files: | tee -a $HOME/.backup_log.txt 
  elif [[ "$1" == "full" ]]; then
    rclone sync "$2" -P ~/library pcloud:linux-backup/library | tee -a $HOME/.backup_log.txt 
  fi
}

backup_notes ()
{
  rclone sync $1 --exclude "notes/.git/" --exclude "renv/library/**" --exclude "notes/.filtered-notes.txt" --exclude "notes/.list-of-notes.txt" --exclude ".devbox/" -P ~/notes pcloud:linux-backup/notes | tee -a $HOME/.backup_log.txt
}


backup_personal ()
{
  rclone sync "$1" -P ~/music pcloud:linux-backup/music | tee -a $HOME/.backup_log.txt 
  rclone copy "$1" -P ~/pictures/wallpapers pcloud:linux-backup/pictures/wallpapers | tee -a $HOME/.backup_log.txt 
}

restore_core ()
{
  rclone copy "$1" -P pcloud:linux-backup/.calcurse ~/.local/share/calcurse 
  rclone copy "$1" -P pcloud:linux-backup/.lazy-nvim-backup ~/.lazy-nvim-backup
  rclone copy "$1" -P pcloud:linux-backup/code ~/code
  rclone copy "$1" -P pcloud:linux-backup/documents ~/documents
  rclone copy "$1" -P pcloud:linux-backup/work ~/work
  rclone copy "$1" -P pcloud:linux-backup/notes ~/notes
}

restore_workstation ()
{
  restore_core "$1"
  rclone copy "$1" -P pcloud:linux-backup/library/_files/_covers/textbooks ~/library/_files/_covers/textbooks
  rclone copy "$1" -P pcloud:linux-backup/library/_files/cheatsheets ~/library/_files/cheatsheets
  rclone copy "$1" -P pcloud:linux-backup/library/_files/courses ~/library/_files/courses
  rclone copy "$1" -P pcloud:linux-backup/library/_files/misc-docs ~/library/_files/misc-docs
  rclone copy "$1" -P pcloud:linux-backup/library/_files/papers ~/library/_files/papers
  rclone copy "$1" -P pcloud:linux-backup/library/_files/roadmaps ~/library/_files/roadmaps
  rclone copy "$1" -P pcloud:linux-backup/library/_files/shortbooks ~/library/_files/shortbooks
  rclone copy "$1" -P pcloud:linux-backup/library/_files/talks ~/library/_files/talks
  rclone copy "$1" -P pcloud:linux-backup/library/_files/textbooks ~/library/_files/textbooks
  rclone copy "$1" -P pcloud:linux-backup/library/_files/references.bib ~/library/_files/references.bib
  rclone copy "$1" -P pcloud:linux-backup/pictures/wallpapers ~/pictures/wallpapers
  # Nice music for work/coding
  rclone copy "$1" -P pcloud:linux-backup/music/Adnan_Joubran ~/music/Adnan_Joubran
  rclone copy "$1" -P pcloud:linux-backup/music/Avishai_Cohen ~/music/Avishai_Cohen
  rclone copy "$1" -P pcloud:linux-backup/music/Avishai_Cohen_Trio ~/music/Avishai_Cohen_Trio
  rclone copy "$1" -P pcloud:linux-backup/music/Ballake_Sissoko ~/music/Ballake_Sissoko
  rclone copy "$1" -P pcloud:linux-backup/music/Ballake_Sissoko_and_Vincent_Segal ~/music/Ballake_Sissoko_and_Vincent_Segal
  rclone copy "$1" -P pcloud:linux-backup/music/Bela_Fleck_and_Toumani_Diabate ~/music/Bela_Fleck_and_Toumani_Diabate
  rclone copy "$1" -P pcloud:linux-backup/music/Derek_Gripper ~/music/Derek_Gripper
  rclone copy "$1" -P pcloud:linux-backup/music/Electric_Octopus ~/music/Electric_Octopus
  rclone copy "$1" -P pcloud:linux-backup/music/Faran ~/music/Faran
  rclone copy "$1" -P pcloud:linux-backup/music/Gogo_penguin ~/music/Gogo_penguin
  rclone copy "$1" -P pcloud:linux-backup/music/Gustavo_Santaolalla ~/music/Gustavo_Santaolalla
  rclone copy "$1" -P pcloud:linux-backup/music/No_Naime ~/music/No_Naime
  rclone copy "$1" -P pcloud:linux-backup/music/OST ~/music/OST
}

sync_workstation ()
{

  # ~/code
  rclone sync "$1" -P --exclude ".git/" --exclude "renv/library/**" --exclude ".devbox/" \
    pcloud:linux-backup/code ~/code
  # ~/documents
  rclone sync "$1" -P pcloud:linux-backup/documents ~/documents
  # ~/library/_files
  # backup only files useful for work
  rclone sync "$1" -P pcloud:linux-backup/library/_files/_covers/textbooks ~/library/_files/_covers/textbooks | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P pcloud:linux-backup/library/_files/cheatsheets ~/library/_files/cheatsheets | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P pcloud:linux-backup/library/_files/courses ~/library/_files/courses | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P pcloud:linux-backup/library/_files/misc-docs ~/library/_files/misc-docs | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P pcloud:linux-backup/library/_files/papers ~/library/_files/papers | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P pcloud:linux-backup/library/_files/roadmaps ~/library/_files/roadmaps | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P pcloud:linux-backup/library/_files/shortbooks ~/library/_files/shortbooks | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P pcloud:linux-backup/library/_files/talks ~/library/_files/talks | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P pcloud:linux-backup/library/_files/textbooks ~/library/_files/textbooks | tee -a $HOME/.backup_log.txt 
  rclone sync "$1" -P pcloud:linux-backup/library/_files/references.bib ~/library/_files/references.bib | tee -a $HOME/.backup_log.txt 

  # ~/notes
  rclone sync $1 --exclude "notes/.git/" --exclude "renv/library/**" --exclude ".devbox/" \
    --exclude "notes/.filtered-notes.txt" --exclude "notes/.list-of-notes.txt" \
    -P pcloud:linux-backup/notes ~/notes | tee -a $HOME/.backup_log.txt

  # ~/work
  rclone sync "$1" -P --exclude ".git/**" --exclude "renv/library/**" --exclude ".devbox/" \
      pcloud:linux-backup/work ~/work | tee -a $HOME/.backup_log.txt 

}

restore_full ()
{
  restore_core "$1"
  rclone copy "$1" -P pcloud:linux-backup/pictures ~/pictures
  rclone copy "$1" -P pcloud:linux-backup/music ~/music
}


#------------------------------------------------------------------------------
#  Main program
#------------------------------------------------------------------------------

main () {

case "$1" in

  # NB: "${2:---check-first}" is a trick to avoid too many arguments error when
  # I run the script without a "--dry-run" flag

  notes)
    backup_notes "${2:---check-first}"
    ;;

  workstation)
    backup_hidden_dirs "${2:---check-first}"
    backup_code_repos "${2:---check-first}" 
    backup_docs_and_work "${2:---check-first}"
    backup_library workstation "${2:---check-first}"
    # backup_notes "${2:---check-first}"
    ;;

  full)
    # backup_hidden_dirs "${2:---check-first}"
    # backup_code_repos "${2:---check-first}"
    # backup_docs_and_work "${2:---check-first}"
    # backup_notes "${2:---check-first}"
    backup_library full "${2:---check-first}"
    backup_personal "${2:---check-first}"
    ;;

  restore-workstation)
    restore_workstation "${2:---check-first}"
    ;;

  restore-full)
    restore_full "${2:---check-first}"
    ;;

  # sync-workstation)
  #   sync_workstation "${2:---check-first}"
  #   ;;

  *)
    echo "Provide a backup ('workstation', 'notes', 'full') or restore ('restore-workstation', 'restore-full') option."
    ;;
esac

}

main "$@"
