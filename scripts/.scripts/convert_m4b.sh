#!/bin/bash


# sudo apt-get install id3v2 ffmpeg
#
# USAGE: 
# cd /book title/
# bash ~/this_script_path.sh
# rm *.m4b (you need to manually remove the original in case something goes wrong)
#
#
# EXAMPLE:
#
#   $ ls
#   The Wheel of Time - Book 11 - Knife of Dreams, Part 1.m4b  The Wheel of Time - Book 11 - Knife of Dreams, Part 2.m4b
#   $ ~/convert_m4b.sh
#   .....
# 
#   [OUTPUT]:
#
#   File: Knife of Dreams - Chapter 18 - News for the Dragon.mp3
#   Metadata: ID3v2.3
#   Title: Chapter 18 - News for the Dragon
#   Artist: Robert Jordan
#   Album: Knife of Dreams
#   Track: 23
#   Genre: Audiobooks

#initial track number
track=1

#assumes ( if there are multiple files Part 01/Part 02) that they are in alphabetical order
for i in *.m4b;
do
    name=`echo $i | cut -d'.' -f1`;
    echo $name;
    ffmpeg -i "$i" -acodec libmp3lame -ar 22050 -ab 64k "$name.mp3"
    full_file_path="$name.mp3"

    #split chapters
    title=$(pwd | sed 's,^\(.*/\)\?\([^/]*\),\2,' | cut -d , -f 1)

    ffmpeg -i "$full_file_path" 2> tmp.txt

    while read -r first _ _ start _ end; do
        if [[ "${first}" = "Chapter" ]]
        then
            read  # discard line with Metadata:
            read _ _ chapter
            chapter=$(sed -re ":r;s/\b[0-9]{1,$((1))}\b/0&/g;tr" <<<$chapter)
            chapter_file="${title} - ${chapter}.mp3"
            echo "processing $chapter"
            </dev/null ffmpeg -loglevel error -stats -i "${full_file_path}" -ss "${start%?}" -to "${end}" -codec:a copy -metadata track="${chapter}" "${chapter_file}"
            id3v2 --song "$chapter" "$chapter_file"
            id3v2 --album "$title" "$chapter_file"
            id3v2 --track "$track" "$chapter_file"
            echo "$title - $chapter"
            track=$((track+1))
        fi
    done <tmp.txt

    rm tmp.txt
    # rm "$full_file_path"

done;

# ----------------

# Sources:
# https://unix.stackexchange.com/questions/499179/using-ffmpeg-to-split-an-audible-audio-book-into-chapters
# https://gist.github.com/nitrag/a188b8969a539ce0f7a64deb56c00277
# https://unix.stackexchange.com/questions/1670/how-can-i-use-ffmpeg-to-split-mpeg-video-into-10-minute-chunks
# https://blog.programster.org/ffmpeg-too-many-packets-buffered-for-output-stream

#INPUT="$1"
#METADATA="audiobook_chapters.json"
#
#ffprobe -i "$INPUT" -print_format json -show_chapters > $METADATA 2> /dev/null
#
#NB_CHAPTERS="$(jaq '.chapters | length' audiobook_chapters.json)"
#
#ffmpeg -i $INPUT -ss 0.000000 -to 819.059000 "part_0.mp3"
# for nb in $(seq 0 $(( $NB_CHAPTERS - 1 ))); do
#     start=$(jaq ".chapters[$nb].start" $METADATA)
#     end=$(jaq ".chapters[$nb].end_time" $METADATA | sd '[".]' '' | sd '0+$' '') 
#     ffmpeg -i $INPUT -ss $start -to $end "part_${nb}.mp3"
# done

# SANDBOX:
# jaq ".chapters[2].start" audiobook_chapters.json
# jaq ".chapters | length" audiobook_chapters.json
# in for loop:
# echo $(jaq ".chapters[$nb].tags.title" $METADATA)

# ffmpeg -i 04_Book_01_Chapter_03.mp3 -c copy -map 0 -segment_time 00:30:00 -f segment _queue/chap03_%03d.mp3
# ffmpeg -i 03_Book_01_Chapter_02.mp3 -to 00:20:00 -codec:a copy _queue/chap02_start_cut.mp3
# ffmpeg -i 03_Book_01_Chapter_02.mp3 -ss 00:19:59 -codec:a copy tmp.mp3
# ffmpeg -i tmp.mp3 -c copy -map 0 -segment_time 00:30:00 -f segment _queue/chap02_%03d.mp3
