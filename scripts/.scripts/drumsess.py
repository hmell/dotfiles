#!/usr/bin/env python3

import argparse
import pandas as pd

prog_desc = """ Creates a practice playlist for drum sessions.
A number of randomly selected songs from practice.csv are combined based on
the type and count parameters provided for each target."""

# Command Line parsing
parser = argparse.ArgumentParser(description = prog_desc)

parser.add_argument("-t", "--table", help="""Print a table giving the mapping
                    between codes and types of exercices""",
                    action = "store_true")
parser.add_argument("-l", "--left-hand", help="""Take the type of exercise and the
                    number of songs to be selected for the left hand.""",
                    nargs=2, metavar=("TYPE", "COUNT"), action = "append")

parser.add_argument("-r", "--right-hand", help="""Take the type of exercise and
                    the number of songs to be selected for the right hand.""",
                    nargs=2, metavar=("TYPE", "COUNT"), action = "append")

parser.add_argument("-k", "--killer-exercise", help="""Take the type of killer
                    exercise and the number of songs to be selected.""",
                    nargs=2, metavar=("TYPE", "COUNT"), action = "append")

parser.add_argument("-f", "--foot", help="""Take the type of exercise and the
                    number of songs to be selected for the right foot.""",
                    nargs=2, metavar=("TYPE", "COUNT"), action = "append")

args = parser.parse_args()
arg_list = ["left_hand", "right_hand", "killer_exercise", "foot"]

# Functions


def exo_type_from_code(code, mappings):
    exo_type = mappings[code]
    return exo_type


# Create a drumsess.txt playlist
input_path = '$HOME/Documents/music/drums/practice_songs.csv'
output_path = '$HOME/Documents/music/drums/drumsession.csv'

songs_list = pd.read_csv(input_path)
codes_dict = {
    "8ns":  "8th notes - slow",
    "8nm":  "8th notes - medium",
    "8nf":  "8th notes - fast",
    "hs":   "Hands - slow",
    "hm":   "Hands - medium",
    "hf":   "Hands - fast",
    "frs":  "Foot and right hand - slow",
    "frm":  "Foot and right hand - medium",
    "frf":  "Foot and right hand - fast",
    "fls":  "Foot and left hand - slow",
    "flm":  "Foot and left hand - medium",
    "flf":  "Foot and left hand - fast",
    "dbs":  "Doubles - slow",
    "dbm":  "Doubles - medium",
    "dbf":  "Doubles - fast",
    "gs":   "Groupings - slow",
    "gm":   "Groupings - medium",
    "gf":   "Groupings - fast",
}

drumsession = pd.DataFrame()
for arg in vars(args):
    if (vars(args)[arg] is not None) and arg not in ["table"]:
        for exo in vars(args)[arg]:
            target = arg.title().replace('_', ' ')
            songs_target = songs_list.loc[songs_list.Target == target, ]
            full_type = exo_type_from_code(exo[0], codes_dict)
            count = int(exo[1])
            filtered_songs = songs_target.loc[songs_target.Type == full_type, ]
            sampled_songs = filtered_songs.sample(n = count)
            drumsession = drumsession.append(sampled_songs)
if args.table:
    print(f"\nTypes of exercices and codes for command line options:\n")
    for k, v in codes_dict.items():
        print(f"{v:29} ->   {k}")
    print("\n")


drumsession.to_csv(output_path, index = False)







