#!/bin/bash

playlist="~/.config/mpv/mpv_playlist.txt"
# Take optional prefix for file paths to append
if [ ! -z "$2" ] ; then
  prefix=$(echo "$2" | sed 's# | #/#g' | tr ' ' '_')
fi

fzf -m --tac --cycle --prompt "Tracklist> " \
  --bind "alt-a:select-all,alt-d:deselect-all" \
  --bind "ctrl-alt-u:last,ctrl-alt-b:first" \
  --bind "alt-y:execute(echo {+} | tr ' ' '\n' | sed -e 's#^#${prefix}/#' | sort --numeric-sort | tee -a $1),alt-bspace:execute(:>| $1)" \
  --bind "ctrl-alt-j:execute(echo {+} | tr ' ' '\n' | sed -e 's#^#${prefix}/#' | sort --numeric-sort | mpv --playlist=- )" \
  --bind 'ctrl-alt-p:execute(clear_screen)+execute(mpv --playlist=$HOME/.config/mpv/playlist.txt)' \
  --bind '?:execute@clear_screen@+preview(bat --style=plain --color=always ~/.scripts/fzf-music/help.md)'
