#!/bin/bash

# Custom preview function using fzfub and called in fzf-img-music.sh
fzfm_draw_preview ()
{
  # Get path to image cover from custom formatted input text in fzf-music
  IMG_PATH=$(musiclib covers "$@")
  echo "$IMG_PATH" > ~/.scripts/l.txt
  ueberzugpp cmd -s "$(cat $HOME/.local/share/fzfm-ueberzugpp-socket)" -i fzfmpreview -a add -x $X -y 1 --max-width $FZF_PREVIEW_COLUMNS --max-height $FZF_PREVIEW_LINES -f "${IMG_PATH}"
}

# Clear preview in fzf-music
fzfm_clear_screen ()
{
  ueberzugpp cmd -s "$(cat $HOME/.local/share/fzfm-ueberzugpp-socket)" -i fzfmpreview -a remove
}

case "$1" in
    --preview)
      shift
      fzfm_draw_preview "$@"
    ;;
    --clear)
      fzfm_clear_screen
    ;;
esac
