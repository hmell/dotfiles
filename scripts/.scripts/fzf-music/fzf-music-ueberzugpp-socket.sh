#!/bin/bash
UB_PID_FILE="/tmp/.$(uuidgen)"
ueberzugpp layer --no-opencv --no-cache --no-stdin --silent --pid-file $UB_PID_FILE
UB_PID=$(cat $UB_PID_FILE)

SOCKET=/tmp/ueberzugpp-$UB_PID.socket
echo $SOCKET > $HOME/.local/share/fzfm-ueberzugpp-socket
