#!/bin/bash

prefix="$HOME/music"
playlist="$HOME/.config/mpv/playlist.txt"

# code to configure preview with ueberzugpp
UB_PID_FILE="/tmp/$(uuidgen)"
ueberzugpp layer --no-opencv --no-cache --no-stdin --silent --pid-file $UB_PID_FILE
UB_PID=$(cat $UB_PID_FILE)

SOCKET=/tmp/ueberzugpp-$UB_PID.socket
export X=$(($(tput cols) / 2 + 2))

# save socket path in file to facilitate communication from other scripts
echo $SOCKET > $HOME/.local/share/fzfm-ueberzugpp-socket

# main
musiclib albums | \
    # fzf --preview "fzfm_draw_preview {}"   \
    fzf --preview "$HOME/.scripts/fzf-music/fzf-img-musiclib.sh --preview {}"   \
    --multi \
    --cycle \
    --prompt "Albums> " \
    --bind "alt-a:select-all,alt-d:deselect-all+toggle-preview+toggle-preview" \
    --bind "ctrl-alt-u:last,ctrl-alt-b:first" \
    --bind "alt-q:replace-query" \
    --bind "µ:execute@$HOME/.scripts/fzf-music/fzf-img-musiclib.sh --clear@" \
    --bind "shift-up:preview-half-page-up,shift-down:preview-half-page-down" `# MODE SWITCHING:` \
    --bind 'alt-r:change-prompt(Albums> )+reload(musiclib albums)' \
    --bind 'alt-t:change-prompt(Songs> )+reload(musiclib titles)' \
    --bind "alt-b:change-prompt(Artists> )+reload(musiclib artists)" \
    --bind "alt-f:change-prompt(Discography> )+reload(musiclib discography {})" \
    --bind 'alt-m:change-prompt(Songs MPV Playlist> )+reload(cat $HOME/.config/mpv/playlist.txt)' `# PLAY / CLEAR MPV PLAYLIST:` \
    --bind 'ctrl-alt-p:execute@$HOME/.scripts/fzf-music/fzf-img-musiclib.sh --clear@+execute(mpv --playlist=$HOME/.config/mpv/playlist.txt)' \
    --bind 'alt-bspace:execute(:>| $HOME/.config/mpv/playlist.txt)' `# TRACKS MODE:` \
    --bind 'ctrl-alt-j:execute@$HOME/.scripts/fzf-music/fzf-img-musiclib.sh --clear@+execute@for var in {+} ; do echo "$var" | sed  -e "s# | #/#g" | tr " " "_"  ; done | sed -e "s#^#$HOME/music/#g" | mpv --playlist=- @' \
    --bind 'alt-y:execute@for var in {+} ; do echo "$var" | sed  -e "s# | #/#g" | tr " " "_"  ; done | sed -e "s#^#$HOME/music/#g" | tee -a $HOME/.config/mpv/playlist.txt @' \
    --bind 'ctrl-alt-s:execute@$HOME/.scripts/fzf-music/fzf-img-musiclib.sh --clear@+execute@$HOME/.scripts/fzf-music/fzf-playlists.sh@' \
    --bind 'ctrl-s:execute@$HOME/.scripts/fzf-music/fzf-img-musiclib.sh --clear@+execute@musiclib paths {} | $HOME/.scripts/fzf-music/fzf-append-fp.sh $HOME/.config/mpv/playlist.txt $HOME/music/{}@' `# PLAYLIST MODE` \
    --bind 'ctrl-d:execute@for var in {+} ; do echo "$var" | xargs -I fp sed -i "s:fp:FZFDELETED:" ~/.config/mpv/playlist.txt ; done ; cat ~/.config/mpv/playlist.txt | grep --invert-match "FZFDELETED" | tee $HOME/.config/mpv/playlist.txt@+reload(cat ~/.config/mpv/playlist.txt)' `# HELP menu with keybindings and their main effect:` \
    --bind '?:execute@$HOME/.scripts/fzf-music/fzf-img-musiclib.sh --clear@+preview(bat --style=plain --color=always ~/.scripts/fzf-music/help.md)'

ueberzugpp cmd -s "$(cat $HOME/.local/share/fzfm-ueberzugpp-socket)" -a exit
rm $HOME/.local/share/fzfm-ueberzugpp-socket
