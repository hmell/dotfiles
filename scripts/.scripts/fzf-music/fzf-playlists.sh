#!/bin/bash

ls ~/.config/mpv/playlists/*.txt | fzf -m --cycle --prompt "Playlists> " \
  --bind "alt-a:select-all,alt-d:deselect-all,ctrl-alt-u:last,ctrl-alt-b:first" \
  --bind 'alt-m:change-prompt(Songs MPV Playlist> )+reload(cat $HOME/.config/mpv/playlist.txt)' \
  --bind 'alt-bspace:execute(:>| $HOME/.config/mpv/playlist.txt)' \
  --bind 'ctrl-d:execute@for var in {+} ; do echo "$var" | xargs -I fp sed -i "s:fp:FZFDELETED:" ~/.config/mpv/playlist.txt ; done ; cat ~/.config/mpv/playlist.txt | grep --invert-match "FZFDELETED" | tee $HOME/.config/mpv/playlist.txt@+reload(cat ~/.config/mpv/playlist.txt)' \
  --bind 'alt-y:execute@for var in {+} ; do echo "$var" | sed  -e "s# | #/#g" | tr " " "_"  ; done | sed -e "s#^#$HOME/music/#g" | tee -a $HOME/.config/mpv/playlist.txt @' \
  --bind "ctrl-alt-j:execute(echo {+} | tr ' ' '\n' | sed -e 's#^#${prefix}/#' | sort --numeric-sort | mpv --playlist=- )" \
  --bind 'ctrl-alt-p:execute(clear_screen)+execute(mpv --playlist=$HOME/.config/mpv/playlist.txt)' \
  --bind 'ctrl-alt-l:execute(clear_screen)+execute(mpv --playlist={})' \
  --bind 'alt-enter:execute@cp $HOME/.config/mpv/playlist.txt $HOME/.config/mpv/playlists/{q}.txt@+change-prompt(Test> )+reload(ls $HOME/.config/mpv/playlists/*.txt)+clear-query' \
  --bind 'alt-p:change-prompt(Playlists> )+reload(ls $HOME/.config/mpv/playlists/*.txt)' \
  --bind 'alt-s:change-prompt(Songs playlist> )+reload(cat {})' \
  --bind 'ctrl-alt-x:+execute(rm {})+reload(ls $HOME/.config/mpv/playlists/*.txt)' \
  --bind "ctrl-o:execute@cp {} ~/.config/mpv/playlist.txt@+reload(ls $HOME/.config/mpv/playlists/*.txt)+change-prompt(Playlists> )+clear-query" \
  --bind "ctrl-alt-y:execute@cat {} >> ~/.config/mpv/playlist.txt@+reload(ls $HOME/.config/mpv/playlists/*.txt)+change-prompt(Playlists> )+clear-query" \
  --bind "ctrl-m:execute@cp ~/.config/mpv/playlist.txt {} @+reload(ls $HOME/.config/mpv/playlists/*.txt)+change-prompt(Playlists> )+clear-query" \
  --bind "ctrl-alt-k:execute@cat ~/.config/mpv/playlist.txt >> {} @+reload(ls $HOME/.config/mpv/playlists/*.txt)+change-prompt(Playlists> )+clear-query" \
  --bind '?:execute@clear_screen@+preview(bat --style=plain --color=always ~/.scripts/fzf-music/help.md)'
