## LIST OF BINDINGS

### MISCELLANEOUS

    alt-a       -  Select all
    alt-d       -  Deselect all
    alt-q       -  Replace query with current selection
    shift-up    -  Preview half page up
    shift-down  -  Preview half page down
    ctrl-alt-u  -  Top match
    ctrl-alt-b  -  Bottom match
    µ           -  Redraw command

### PLAY

    ctrl-alt-j  -  Play selection
    ctrl-alt-p  -  Play mpv playlist
    ctrl-alt-l  -  Play selected playlist

### MODE SWITCHING

    alt-t        -  Reload with tracks
    alt-r        -  Reload with albums
    alt-b        -  Reload with artists
    alt-f        -  Reload with discography
    alt-m        -  Reload with mpv playlist
    ctrl-s       -  Reload with tracks of albums or artist
                    depending on context
    ctrl-alt-s   -  Reload with playlists

### MANAGE MPV PLAYLIST

    alt-bspace  -  Clear mpv playlist
    alt-y       -  Add selected tracks to mpv playlist
    ctrl-d      -  Remove selected files/folders from playlist

### MANAGE SAVED PLAYLISTS

    alt-p         -  Reload with all playlists
    alt-s         -  Reload with songs of selected playlist
    alt-enter     -  Save mpv playlist with title in query (no ext)
    ctrl-alt-x    -  Delete  selected playlist

    <!-- Selected playlist -> Mpv playlist -->
    ctrl-o        -  Replace mpv playlist with selected playlist
    ctrl-alt-y    -  Append selected playlist to mpv playlist

    <!-- Mpv playlist -> selected playlist -->
    ctrl-m        -  Replace selected playlist with mpv playlist
    ctrl-alt-k    -  Append mpv playlist to selected playlist


