#!/bin/bash
echo $1

fzf-ueberzogen.sh --phony --header="<C-m> to go back" --bind 'ctrl-m:execute@>"$(ls -t /tmp/tmp.*fzf*-ueberzug | head -n 1)" declare -Ap add_command=([action]="remove" [identifier]="preview")@+abort'
