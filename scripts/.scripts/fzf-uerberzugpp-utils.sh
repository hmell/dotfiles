#!/bin/bash

# Custom preview function using fzfub and called in fzf-img-music.sh
fzfm_draw_preview ()
{
  # Get path to image cover from custom formatted input text in fzf-music
  IMG_PATH=$(musiclib covers "$@")
  echo "$IMG_PATH" > ~/.scripts/l.txt
  ueberzugpp cmd -s "$(cat $HOME/.local/share/fzfm-ueberzugpp-socket)" -i fzfmpreview -a add -x $X -y 1 --max-width $FZF_PREVIEW_COLUMNS --max-height $FZF_PREVIEW_LINES -f "${IMG_PATH}"
}

# 
fzfm_clear_screen ()
{
  ueberzugpp cmd -s "$(cat $HOME/.local/share/fzfm-ueberzugpp-socket)" -i fzfmpreview -a remove
}

ueberzugpp_clear_all ()
{
  # while IFS= read -r item
  #   do
  #     ueberzugpp cmd -s "$item" -a remove
  #   done < <(ls /tmp/ueberzugpp-*.socket)
  # ls /tmp/ueberzugpp-*.socket | tail -n 1 | xargs -I{} ueberzugpp cmd -s {} -a remove
  fd -g 'ueberzugpp-*.socket' /tmp -x ueberzugpp cmd -s {} -a remove
}

case "$1" in
    --fzfm-preview)
      shift
      fzfm_draw_preview "$@"
    ;;
    --fzfm-clear)
      fzfm_clear_screen
    ;;
    --clear-all)
      ueberzugpp_clear_all
    ;;
esac
