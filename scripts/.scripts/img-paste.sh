#! /bin/bash
mkdir -p "$HOME/notes/supp/${1}/img/"
echo "${1}"
IMG=$(ls -ltr $HOME/*.png | tail -n 1 | rev | cut -d ' ' -f 1 | rev)
PATH="$HOME/notes/supp/${1}/img/${1}_img$(( $(ls $HOME/notes/supp/${1}/img/${1}* 2> /dev/null | wc -l) + 1 )).png"

# Move screenshot to image folder and rename it
/bin/mv $IMG $PATH
