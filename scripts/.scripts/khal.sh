#!/bin/bash

add_event ()
{
    task_summary="$(gum input)"
    khal new -a "$1" today "$task_summary"
}

main () {

  case "$1" in
  
    --hell)
      add_event "hell"
      ;;
  
    *)
        echo "Specify a target calendar (e.g. --hell)."
      ;;
  esac

}

main "$@"
