#! /bin/bash

PID=$(ps -aux | rg live-server | grep -v 'rg ' | tr -s ' ' | cut -f2 -d' ')
kill -9 $PID
