#!/bin/bash

# DEFAULT_DECK_ID="20230109065932"
DEFAULT_DECK_ID="20250106074505"
SPANISH_DECK_ID="20230106204755"

today="$(date +%Y%m%d%H%M%S)"



log_cards_session ()
{

    if [[ $1 == "before" ]]; then

        :> ../.log-cards-before.txt
        for c in $3; do
            rg -N "Card: $c" "$2" >> ../.log-cards-before.txt
        done

    elif [[ $1 == "after" ]]; then

        :> ../.log-cards-after.txt
        for c in $3; do
            rg -N "Card: $c" "$2" >> ../.log-cards-after.txt
        done
        
    fi

    
}



clean_up ()
{

    # Kill live-server
    ps ax | grep live-server \
        | sed 's/^\s*//' \
        | cut -f1 -d' ' | head -n 1 \
        | xargs -I{} kill -9 {}

}



get_due_cards_ids ()
{

    awk -v today="$(date -d $(date +%Y-%m-%d) +%s)" -F '/' \
        '/^Card: / {if ( ($2 - today < 0 || $2 - today == 0) && ($2 > 0)) print $0 }' "$1" \
        | cut -d" " -f 2 \
        | tr '\n' ' ' \
        | sed 's/.$//'

}



add_new_cards ()
{

    for i in $(seq 1 $2) ; do
        card_id="$(rg -N ' / 0 / ' $1 | head -n 1 | cut -d'/' -f'1' | cut -d' ' -f2)"
        new_revision_date=$(date -d $(date +%Y-%m-%d) +%s)
        newline="Card: $(echo ${card_id} / ${new_revision_date} / 0)"
        echo $card_id
        echo "-- $newline"
        sed -i "s,Card: ${card_id}.*$,$newline,g" "$1"
    done

}



generate_audio_sample ()
{
    card_collection="$1"
    card_id="$2"
    deck_id="$3"
    fp="$HOME/notes/supp/${deck_id}/audio/${card_id}.mp3"

    if [[ ! -a "$fp" ]]; then
    
      awk -v target="$card_id" '
$0 ~ "^Card: " target {
  flag=1
  next
}
flag && /audio/ {
  print $0
  flag=0
  next
}
{
  next
}
' "$1" \
      | cut -d'>' -f2 | cut -d'<' -f1 \
      | xargs -I{} gtts-cli -l es "{}" --output "audio/${card_id}.mp3"

      if [[ "$?" -eq 0 ]]; then
        gum format -- "* audio sample for $card_id completed"
      fi
        
    fi

}



create_html_flashcard ()
{

    awk -v target="$2" '
$0 ~ "^Card: " target {
  flag=1
  next
}
/----------/ {
  flag=0
  next
}
flag{
  print $0
  next
}
{
  next
}
' "$1" | cat - <(echo "<section><p><i>$3 / $4</i></p></section>") | pandoc --lua-filter codeblock.lua -f markdown+fenced_divs -t html -s --include-in-header $HOME/.pandoc/kukariri.js -o tmp/tmp-file.html

}



get_card_info ()
{

    if [[ "$3" == "revision" ]]; then
        rg -N "Card: $2" "$1" | cut -d'/' -f'2'
    elif [[ "$3" == "interval" ]]; then
        rg -N "Card: $2" "$1" | cut -d'/' -f'3'
    fi
    
}



get_next_interval ()
{

    case "$1" in
        0) echo 1
        ;;
        1) echo 4
        ;;
        4) echo 7
        ;;
        7) echo 14
        ;;
        14) echo 30
        ;;
        30) echo 60
        ;;
        60) echo 90
        ;;
        90) echo 180
        ;;
        *) echo 180
        ;;
    esac
    
}



get_prev_interval ()
{

    case "$1" in
        1) echo 0
        ;;
        4) echo 1
        ;;
        7) echo 4
        ;;
        14) echo 7
        ;;
        30) echo 14
        ;;
        60) echo 30
        ;;
        90) echo 60
        ;;
        180) echo 90
        ;;
        *) echo 0
        ;;
    esac
    
}



get_feedback ()
{

    # Choices for new interval based on current value
    down_choice="$(get_prev_interval $1)"
    up_choice="$(get_next_interval $1)"

    # Get user feedback
    if [[ "$1" -eq 0 ]]; then
        gum choose $1 $up_choice
    elif [[ "$1" -eq 1 ]]; then
        gum choose $down_choice $1 $up_choice
    elif [[ "$1" -eq 180 ]]; then
        gum choose 0 $down_choice $1
    else
        gum choose 0 $down_choice $1 $up_choice
    fi

}



update_card_info ()
{
    
    card_collection="$1"
    card_id="$2"
    new_interval="$3"
    # revision_date="$(get_card_info $card_collection $card_id revision)"
    today="$(date -d $(date +%Y-%m-%d) +%s)"

    # compute new revision date(86400 = 24h in format for comparisons)
    new_revision_date="$(( $today + $new_interval * 86400 ))"

    newline="Card: $(echo ${card_id} / ${new_revision_date} / ${new_interval})"
    sed -i "s,Card: $card_id.*$,$newline,g" "$card_collection"
    
}



start_session ()
{

    deck_ID="$1"
    target_dir="$HOME/notes/supp/$1"
    cd $target_dir
    # card_collection="$HOME/notes/notes/${1}.md"
    card_collection="${target_dir}/deck.md"

    # Start live preview of template flashcard
    cp ./tmp/flashcard-template.html ./tmp/flashcard.html
    live-server --open=tmp/flashcard.html > /dev/null 2>&1 &

    # Identify new cards
    total_new_cards="$(rg -N ' / 0 / ' $card_collection | wc -l)"

    # Identify cards that should be reviewed
    cards_ids="$(get_due_cards_ids $card_collection)"
    if [[ -z "$cards_ids" ]]; then
        total_due_cards=0
    else
        total_due_cards="$(echo "$cards_ids" | tr ' ' '\n' | wc -l)"
    fi

    # Select number of cards to review
    gum format -- "# -- Starting rewiew session --" \
        "## You have $total_due_cards due cards and $total_new_cards unreviewed cards." \
        "- Choose number of cards to review (Enter for maximum)"
            total_cards_session="$(gum input --value=$(( $total_due_cards + $total_new_cards )) )"
    gum format -- "You chose to review ${total_cards_session} cards."

    [[ $total_cards_session -lt 1 ]] && exit

    # Complete with new cards
    if [[ "$total_due_cards" -lt "$total_cards_session" ]]; then
        nb_missing_cards="$(( $total_cards_session - $total_due_cards))"
        add_new_cards "$card_collection" $nb_missing_cards
        cards_review="$(get_due_cards_ids $card_collection)"
    else
        cards_review="$(echo $cards_ids | cut -d ' ' -f1-${total_cards_session} )"
    fi

    cards_session="$(echo $cards_review)"

    log_cards_session "before" $card_collection "$cards_session"

    # Generate current HTML flashcard for review
    if [[ "$1" != "$DEFAULT_DECK_ID" ]]; then

        gum format -- "---" "## Generating missing audio samples..."
        for c in $cards_session; do
            generate_audio_sample $card_collection $c $deck_ID
        done
        gum format -- "> All audio samples available!"

    fi

    while [ "$cards_review" ]; do

        cards_new_round=""
        counter=1
        total="$(echo "$cards_review" | tr ' ' '\n' | wc -l)"

        for card in $cards_review; do

            gum format -- "---" "## Generating HTML flashcard..." " "
            create_html_flashcard $card_collection $card $counter $total
            cp ./tmp/tmp-file.html ./tmp/flashcard.html

            # Get feedback from user and update review info
            interval="$(get_card_info $card_collection $card interval)"
            gum format -- "# Reviewing Flashcard $card" \
                "- Select number of days before next review" \
                "(current = ${interval}, 0 to ask again)"
            
            printf "\n"
            new_interval="$(get_feedback $interval)"
            gum format -- "## New interval: $new_interval"

            # Mark for new round or update card info
            if [[ $new_interval -eq 0 ]]; then
                cards_new_round="$cards_new_round $card"
            else
                update_card_info $card_collection $card $new_interval
            fi

            counter=$(( $counter + 1))
                
        done

        cards_review="$(echo $cards_new_round)"
        
    done

    log_cards_session "after" $card_collection "$cards_session"
    clean_up

}



generate_git_grep_pattern ()
{

    tags="$1"
    mode="$2"
    first_tag="$(echo $tags | cut -d ' ' -f1)"
    other_tags="$(echo $tags | cut -d ' ' -f2-)"
    pattern="-e '${first_tag}'"
    for v in $(echo $other_tags) ; do
        pattern="$pattern $mode -e '$v'" 
    done
    echo "$pattern"
    
}



start_tagged_session ()
{

    case "$1" in
        --spanish) tmp_dir="$HOME/notes/supp/${SPANISH_DECK_ID}"
                   cd $tmp_dir
                   card_collection="$HOME/notes/notes/${SPANISH_DECK_ID}.md"
                   deck_ID="$SPANISH_DECK_ID"
        ;;
        *) tmp_dir="$HOME/notes/supp/${DEFAULT_DECK_ID}"
           cd $tmp_dir
           card_collection="$HOME/notes/notes/${DEFAULT_DECK_ID}.md"
           deck_ID="$DEFAULT_DECK_ID"
        ;;
    esac

    # Start live preview of template flashcard
    cp ./tmp/flashcard-template.html ./tmp/flashcard.html
    live-server --open=tmp/flashcard.html > /dev/null 2>&1 &

    # Get tags from user
    gum format -- "# -- Starting rewiew session --"
    tags="$(rg -NI '^<!-- _\|#.*\|_ -->' $card_collection | perl -ne 'print "$1\n" while /(#.*?)\|/g;' | sort -u | gum filter --no-limit | tr '\n' ' ' | sed 's/.$//')"
    nb_tags="$(echo $tags | tr ' ' '\n' | wc -l)"
    # Ask user whether to match any or all tags
    if [[ $nb_tags -gt 1 ]]; then
        gum format -- "## Choose tag matching strategy:"
        matching_mode="$(gum choose 'all' 'any')"
        gum format -- "You chose to match $matching_mode seletected tags."
    fi

    # Get cards ids
    if [[ "$nb_tags" -eq 1 ]]; then
        cards_ids="$(rg -N -A 2 -e "<!-- _.*${tags}.*_ -->" -- $card_collection | rg 'Card: ' | cut -d' ' -f2 | tr '\n' ' ' | sed 's/.$//')"
    else

       # Pattern for git grep --or matching
       if [[ "$matching_mode" == "any" ]]; then
           pattern=$(generate_git_grep_pattern "$tags" '--or')
       else
           pattern=$(generate_git_grep_pattern "$tags" '--and')
       fi
       
       # generate a temporary files with candidates for git grep (no stdin)
       rg -N -A 2 -e '^<!-- _.*_ -->' $card_collection > tmp-tag-candidates.txt
       cards_ids="$(eval git --no-pager grep -h -A 2 $pattern --no-index tmp-tag-candidates.txt | rg 'Card: ' | cut -d' ' -f2 | tr '\n' ' ' | sed 's/.$//')"

    fi

    # Select number of cards to review
    nb_cards="$(echo $cards_ids | tr ' ' '\n' | wc -l)"
    printf '\n'
    gum format -- "## You have $nb_cards cards matching the selected tags." \
        "- Choose number of cards to review (Enter for maximum)"
    total_cards_session="$(gum input --value=$nb_cards)"
    gum format -- "You chose to review ${total_cards_session} cards."
    cards_review="$(echo $cards_ids | cut -d ' ' -f1-${total_cards_session} )"

    cards_session="$(echo $cards_review)"
    log_cards_session "before" $card_collection "$cards_session"


    # Generate current HTML flashcard for review
    if [[ ! -z "$1" ]]; then

        gum format -- "---" "## Generating Audio samples..."
        for c in $cards_session; do
            generate_audio_sample $card_collection $c $deck_ID
        done
        gum format -- "> All audio samples available!"

    fi

    while [ "$cards_review" ]; do

        cards_new_round=""
        counter=1
        total="$(echo "$cards_review" | tr ' ' '\n' | wc -l)"

        for card in $cards_review; do

            gum format -- "---" "## Generating HTML flashcard..." " "
            create_html_flashcard $card_collection $card
            cp ./tmp/tmp-file.html ./tmp/flashcard.html

            # Get feedback from user and update review info
            gum format -- "# Reviewing Flashcard $card" \
                "- Feedback:"
            new_interval="$(gum choose '✓' 'X')"
            printf '\t%s\n' "$new_interval"

            # Mark for new round or update card info
            if [[ $new_interval == "X" ]]; then
                cards_new_round="$cards_new_round $card"
            fi

            counter=$(( $counter + 1))
                
        done

        cards_review="$(echo $cards_new_round)"
        
    done

    log_cards_session "after" $card_collection "$cards_session"
    clean_up

}


#------------------------------------------------------------------------------
#  Main program
#------------------------------------------------------------------------------

main () {

case "$1" in

  -t|--tags)
    start_tagged_session "$2"
    ;;

  --spanish)
    start_session $SPANISH_DECK_ID
    ;;

  *)
    start_session $DEFAULT_DECK_ID
    ;;
esac

}

main "$@"
