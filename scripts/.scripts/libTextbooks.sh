#!/bin/bash

db="$HOME/notes/notes/20211214155811.md"
taglist="$HOME/library/textbooks/_tags.txt"


#------------------------------------------------------------------------------
#  Database
#------------------------------------------------------------------------------


get_unregistered_textbooks () {

  # list textbook paths in folder but not in database file
  comm -23 <(ls $HOME/Library/textbooks/*.{pdf,epub,mobi} | sort) <(rg -N '<.*>' $HOME/notes/notes/20211214155811.md | sed -e 's/\* <//g' | tr -d '>' | sort)

}


append_new_entries_to_database () {

  # Get list of new entries
  entries=$(get_unregistered_textbooks)

  # Select which entries to add
  printf "%s\n" "$entries" | fzf -m | \
    ## append item to db via headless nvim instance
    while read file ; do
      echo "* <${file}>" | xclip -i
      ## ex mode commands to insert path and add empty tag field
      nvim --headless +/BACKLINKS "+set nofoldenable" "+norm 3k" '+exe "r !xclip -o"' +'exe "norm o  _||_\<Left>\<Esc>" | norm o' +"wqa" "$db" 2> /dev/null
      echo $file  -- OK
    done

  # Open database file for tag insertion
  nvim +"/_||_" "+norm zR" "+nohlsearch" "$db"

}


#------------------------------------------------------------------------------
#  Tags
#------------------------------------------------------------------------------


udpate_list_of_tags () {

  # Get unique tag labels and overwrite previous list
  rg -N '_\|.*\|_' $db | tr -d '_' | tr '|' '\n' | sort | uniq | tail -n +3 > $HOME/Library/textbooks/_tags.txt

}


display_tags () {

  if [ "$1" == "-m" ] ; then
    cat $taglist | grep -o -E ".*:" | sed 's/:$//' | sort | uniq
  else
    cat $taglist
  fi

}


rename_tags () {
  : # equivalent to true - can be used as do nothing operator

  # Loop over space-separated list of tags given as input

    ## for each tag label

      ### print old label
      ### ask user for new value
      ### ask for confirmation
      ### substitute old tag with new one in database
      ### print '<old_label> --> <new_label> -- OK '
      ### update taglist

}


delete_tags () {
  :

}


create_tags () {
  :

}


copy_tags () {
  :

}


fzf_tag_menu () {
  :

}


#------------------------------------------------------------------------------
#  Main program
#------------------------------------------------------------------------------

main () {

case "$1" in

  -a|--append)
    append_new_entries_to_database
    ;;

  *)
    echo "$1"
    ;;
esac

}

main "$@"
