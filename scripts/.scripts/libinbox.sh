#!/bin/bash
categories=(  
  books
  courses
  miscellaneous
  pages
  papers
  playlists
  podcasts
  slides
  talks
  textbooks
  tutorials
)

selection=$(printf '%s\n' "${categories[@]}" | fzf)

# Exit if no selection in fzf
if [[ -z "$selection" ]]; then
  exit
fi

newcontent=$(cat <<-EOF
  So, does it work?
  $selection
EOF
)

export selection newcontent
awk '
    1
    $0 == ENVIRON["selection"] {print ENVIRON["newcontent"]}
' ~/library/inbox.md
