#!/bin/bash
selection=$(cat <(printf "_ADD TAGS_\n_EDIT TAGS_\n") ~/library/tags.txt | fzf --tac --multi)

if [[ -z "$selection" ]]; then
  exit
fi

if [[ $(printf "$selection") == "_ADD TAGS_" ]]; then
  nvim -c 'norm Go' +'startinsert' ~/library/tags.txt
  sort -o ~/library/tags.txt{,}
elif [[ $(printf "$selection") == "_EDIT TAGS_" ]]; then
  nvim ~/library/tags.txt
else
  printf "$selection\n" | sed 's/^/#/' | cat <(echo '_') - <(echo '_') | tr '\n' ' ' | tr ' ' '|' | sed 's/.$//'
fi
