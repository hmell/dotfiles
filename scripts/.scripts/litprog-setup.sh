#!/bin/bash

# USAGE: litprog-setup <project_name> <subtitle>

setup ()
{
    devbox init
    
    copier copy --data project_name="$1" --data tag_line="$2" \
        https://github.com/hugomell/template-pandoc .
    
    sd 'echo.*/null' 'poetry install && eval $(poetry env activate)' devbox.json
    
    devbox add python@latest
    devbox add poetry@latest
    
cat << EOF > pyproject.toml
[tool.poetry]
package-mode = false

[tool.poetry.dependencies]
python = "^3.13"
entangled-cli = "^2.1.10"


[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"
EOF
    
    mkdir -p docs/assets/img
    
cat << EOF >> .gitignore

docs/*.md
!docs/index.md
!docs/contents.md
EOF
    
    nvim docs/contents.md docs/index.md notebooks/00-project-workflow.md
}

main ()
{
    if [ $# -eq 0 ]; then
        >&2 echo "Provide a project name (\$1 - required) and an optional tagline (\$2 - optional)."
        exit 1
    fi
    setup "$1" "$2"
}

main "$@"
