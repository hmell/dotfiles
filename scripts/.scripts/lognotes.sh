#! /bin/bash

today=$(date +%F)
year=$(echo $today | cut -f 1 -d "-")
month=$(echo $today | cut -f 2 -d "-")
day=$(echo $today | cut -f 3 -d "-")

noteDir="$HOME/notes/log/${year}/${month}_${year}"
noteFilename="${noteDir}/LOG.md"

lastMod=$(stat -c %y $noteFilename 2> /dev/null | cut -f 1 -d " ")

# Create directory for corresponding month if absent
if [ ! -d $noteDir ]; then
  mkdir -p $noteDir
fi

# Create file for corresponding month if absent
if [ ! -f $noteFilename ]; then
  printf "## ${day}/${month}\n\n" > $noteFilename
elif [ ! $lastMod = $today ]; then
  printf "\n## ${day}/${month}\n\n" >> $noteFilename
fi

nvim -c $ +put_ +foldopen +startinsert $noteFilename


