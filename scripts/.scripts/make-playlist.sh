#!/bin/bash

# Return album covers from list of artists
get_covers () {
    #find "$HOME/Music/$artist/" -iname "cover*" -type f -not -path '*/\.*'
    find "$HOME/music/$artist" \( ! -regex '.*/\..*' \) -type f -iname "*.jpg" -o  \( ! -regex '.*/\..*' \) -type f -iname "*.png"
}

# Erase playlist, select artists from dmenu and display covers with sxiv
:>| ~/.config/mpv/mpv_playlist.txt ; ls ~/Music | dmenu -l 10 -i | while read -r artist ; do get_covers ; done | sort | sxiv -ftib
