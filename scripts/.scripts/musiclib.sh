#!/bin/bash


cd $HOME/music

titles="$(fd -e mp3 -e flac -e m4a -e ape -e ac3 -e wma \
          | sort | sed -e 's:/: | :g' | sed 's/_/ /g')"



print_titles () 
{
  printf "$titles\n"
}



print_albums ()
{
  printf "$titles" | cut -f1,2 -d'|' | uniq | sed -e 's/[[:space:]]*$//'
}



print_artists () {
  printf "$titles" | cut -f1 -d'|' | uniq | sed -e 's/[[:space:]]*$//'
}



print_artists_albums () {
  artist=$(echo "$1" | cut -d'|' -f1)
  print_albums | grep "$artist"
}



get_covers ()
{
  query="$1"
  # Check whether query for band picture or album cover
  if [[ "$(echo $query | awk -F'|' '{print NF}')" -lt 2 ]]; then
    fpath=$(printf "$query" | sed 's/\s*$//' | sed 's/ /_/g')
    fd -a --full-path "${fpath}/000_" $HOME/music/
  else
    fpath=$(printf "$query" | cut -f1,2 -d '|' | sed 's: | :/:g' | sed 's/\s*$//' | sed 's/ /_/g')
    fd -a --full-path "$fpath/cover_art" $HOME/music/
  fi
}



get_album_tracks () {
  album=$(echo "$1" | sed 's# | #/#g' | tr ' ' '_')
  fd -e mp3 -e flac -e m4a -e ape -e wma -e ac3 . $album | cut -f3- -d'/'
}



get_artist_tracks () {
  artist=$(echo "$1" | sed 's# | #/#g' | tr ' ' '_')
  fd -e mp3 -e flac -e m4a -e ape -e wma -e ac3 . $artist | cut -f2- -d'/'
}


main () {

  case "$1" in

    artists)
      print_artists
      ;;

    albums)
      print_albums
      printf $titles
      ;;

    titles)
      print_titles
      ;;

    discography)
      print_artists_albums "$2"
      ;;

    covers)
      get_covers "$2"
      ;;

    paths)
      if [[ $(echo "$2" | awk -F'|' '{print NF}') -gt 1 ]]; then
        get_album_tracks "$2"
      else
        get_artist_tracks "$2"
      fi
      ;;

    *)
      echo "ERROR - specify type of info to retrieve"
      ;;
  esac

}

main "$@"
