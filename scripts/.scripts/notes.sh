#!/bin/bash



# %#% Fzf-menu to select tags for the new entry
add_tags ()
{
  # args: <file with documents info>
  current_tags=$(rg -NI "^tags: \['#.*'\]$" $HOME/notes/notes/ | sd "^tags: \['" "" | tr -d "' ]" | tr "," "\n" | sort -u)

  status="inprogress"
  tag_list=""

  while [ "$status" ]; do
    choice=$(echo "$current_tags" | fzf -m --bind alt-enter:print-query | tr '\n' ' ')
      if [[ -z "$choice" ]]; then
        status=""
      else
        tag_list="$tag_list$choice"
      fi
  done
  # Remove trailing space
  tag_list=$(echo $tag_list | sd '^' '')
  echo "$tag_list"
}



# %#% Output title from a given not ID
get_title_from_id ()
{
    id="$1"
    head "$HOME/notes/notes/${id}.md" -n3 | rg "^title: " | sd "^title: " ""
}



# %#% Create a new note (prompt for title and fuzzy selection of tags)
create_note ()
{
  id="$1"
  title=$(gum input --prompt="Title> ")
  unformatted_tags="$(add_tags)"
  tags=$(echo "$unformatted_tags" | sd " " "', '")

  [ -z "$title" ] || [ -z "$unformatted_tags" ] && exit

  cat << EOF > "$HOME/notes/notes/$id.md"
---
id: $id
title: $title
tags: ['$tags']
---

__TODO__
EOF

}



# %#% Insert link to the given note in nvim buffer
insert_link ()
{
  id="$1"
  nvim_server="$2"
  
  # Get title of the newly created note
  title=$(get_title_from_id $id)
  link="[${title}]($id)"

  # Lua command to paste the link - escaping quotes makes it a bit ugly...
  nvim_command=':lua vim.api.nvim_put({"'"$link"'"}, "c", true, true) <CR>'
  nvim --server "$nvim_server" \
       --remote-send "$nvim_command"
}



# %#% Replace fzf list of notes with only those matching queried tag
reload_after_tag_filtering ()
{
  cat ~/notes/notes/.filtered-notes.txt | \
    xargs -Ifiles head -q -n4 files | \
    sed  "/---$/d" |  \
    sd "^id: |^title: " "" | \
    paste -d"~" - - - > $HOME/notes/notes/.list-of-notes.txt
}



# %#% Restore list of notes (all notes without '#:-old' tag)
reload_notes ()
{
  head -q -n4 $HOME/notes/notes/*.md | \
    sed  '/---$/d' | \
    sd '^id: |^title: ' '' | \
    paste -d'~' - - - | \
    rg -v '#:-old' > $HOME/notes/notes/.list-of-notes.txt
}



# %#% Use ids of filtered notes to generate list of paths
get_paths ()
{
  fzf_output="$@"
  echo "$fzf_output" | \
    tr " " "\n" | \
    rg -INo "[0-9]{14}" | \
    sd "^" "/home/hugo/notes/notes/" | sd "$" ".md" | \
    head -n-1
}



# %#% List unique tags in .list-of-notes.txt
list_tags ()
{
  cat .list-of-notes.txt | \
    cut -f3 -d"~" | \
    tr ',' '\n' | \
    rg -or '$1' "'(.*)'" | \
    sort -u | uniq
}



# %#% Generate pattern for git grep matching from a list of tags
generate_git_grep_pattern ()
{
  tags="$1"
  mode="$2"
  first_tag="$(echo $tags | cut -d ' ' -f1)"
  other_tags="$(echo $tags | cut -d ' ' -f2-)"
  pattern="-e '${first_tag}'"
  for v in $(echo $other_tags) ; do
    pattern="$pattern $mode -e '$v'" 
  done
  echo "$pattern"
}



# %#% Replace .list-of-notes.txt with notes that match all given tags
filter_notes ()
{
  cd ~/notes/notes
  tags=$@
  pattern=$(generate_git_grep_pattern "$tags" '--and')
  matches=$(eval git --no-pager grep --no-index -h $pattern .list-of-notes.txt)
  printf "$matches\n" > $HOME/notes/notes/.list-of-notes.txt
}



# %#% Returns space-separated list of ids from search menu selection
get_ids ()
{
  echo "$@" | \
    tr "~" "\n" | \
    rg -INo "[0-9]{14}" | \
    tr "\n" " " | \
    sd ".$" ""
}



# %#% List filepaths all of all the notes that link to a given note
get_backlinks ()
{
  cd ~/notes/notes
  id="$1"
  rg -l "\]\($id\)" | \
      sd "^" "$HOME/notes/notes/" | head -n-1 | \
      tr '\n' ' ' | sd '.$' ''
}


# %#% Paste image link:
# - Take last image in home folder and move it to current not supp folder 
# - Add a markdown image link to it in calling nvim instance
insert_image ()
{
  id="$1"
  nvim_server="$2"

  mkdir -p "$HOME/notes/supp/${id}/img/"
  img=$(ls -ltr $HOME/*.png | tail -n 1 | rev | cut -d ' ' -f 1 | rev)
  fp="$HOME/notes/supp/${id}/img/${id}_img$(( $(ls $HOME/notes/supp/${id}/img/${id}* 2> /dev/null | wc -l) + 1 )).png"
  
  # Move screenshot to image folder and rename it
  /bin/mv $img $fp

  link="![]($fp)"
  nvim_command=':lua vim.api.nvim_put({"'"$link"'"}, "c", true, true) <CR>'
  nvim --server "$nvim_server" \
       --remote-send "$nvim_command"
}




update_backlinks ()
{
  cd $HOME/notes/notes
  id="$1"
  label="$2"
  notes_backlinks=$(get_backlinks $id | tr ' ' '\n' | rg -INo "[0-9]{14}" | tr "\n" " " | sd ".$" "")
  for note in $(echo $notes_backlinks) ; do
    sed -i -E "s/\[.*\]\($id\)/\[$label\]\($id\)/g" "${note}.md"
  done
}



archive_notes ()
{
  ids=$(get_ids "$@")

  # Stage previous changes to avoid errors
  cd $HOME/notes/notes
  git add .

  # # String preparation for list of archived notes
  header='\nList of notes archived:\n\n'
  archived_notes="$header"

  # Loop over each note ids
  for id in $(echo $ids) ; do
    update_backlinks "$id" "-ARCHIVED-"
    title=$(get_title_from_id "$id")
    note_info="* [${title}](${id})\n"
    archived_notes="${archived_notes}${note_info}"
    git rm -f "${id}.md"
  done

  # Commit changes with informative message
  commit_msg_title="$(gum input --value='archive: ')"
  commit_msg="${commit_msg_title}\n${archived_notes}"
  msg=$(printf "$commit_msg")
  git add .
  git commit -m "$msg"
}



# %#% Prompt for a new title and update all backlinks
update_title ()
{
  cd ~/notes/notes

  id="$1"
  nvim_server="$2"

  old_title=$(get_title_from_id "$id")
  new_title=$(gum input --value="$old_title")

  # Update title in nvim buffer
  sed -i "3s/^.*$/title: ${new_title}/" "${id}.md"
  nvim_command=':lua vim.cmd("e %") <CR>'
  nvim --server "$nvim_server" \
       --remote-send "$nvim_command"

  # Update title in all backlinks
  update_backlinks "$id" "$new_title"
}


#------------------------------------------------------------------------------
#  Main program
#------------------------------------------------------------------------------

main () {

case "$1" in

  -R|--reload)
    reload_notes
    ;;

  -r|--reload-tag-filtering)
    reload_after_tag_filtering
    ;;

  -F|--get-filtered-notes)
    shift
    get_paths "$@" > ~/notes/notes/.filtered-notes.txt
    ;;

  -X|--archive-notes)
    shift
    archive_notes "$@"
    reload_notes
    ;;

  -f|--filter-notes)
    shift
    filter_notes "$@"
    ;;

  -l|--list-tags)
    shift
    list_tags
    ;;

  -P|--get-paths)
    shift
    get_paths "$@"
    ;;

  -n|--open-new-note)
    shift
    create_note $1
    nvim --server "$2" \
       --remote-send ":e $HOME/notes/notes/${id}.md <CR>"
    ;;

  -N|--link-new-note)
    shift
    create_note $1
    insert_link $1 "$2"
    ;;

  -t|--note-title)
    shift
    get_title_from_id $1
    ;;

  -T|--update-title)
    shift
    update_title $1 $2
    ;;

  -L|--insert-link)
    shift
    insert_link $1 $2
    ;;

  -B|--backlinks)
    shift
    get_backlinks $1
    ;;

  -I|--image-link)
    shift
    insert_image $1 $2
    ;;

  *)
    echo "Look at script for various available flags."
    ;;
esac

}

main "$@"
