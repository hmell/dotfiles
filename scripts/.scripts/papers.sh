#!/bin/bash

set -e



papers_tags ()
{
    rg -NI '^tags: _\|#.*\|_' "$1" \
        | perl -ne 'print "$1\n" while /(#.*?)\|/g;' \
        | sort -u \
        | awk '{ printf "~~~~~~~~<placeholder>~~~~~~~~" $0 "\n" }'
}



generate_git_grep_pattern ()
{

    tags="$@"
    first_tag="$(echo $tags | cut -d ' ' -f1)"
    other_tags="$(echo $tags | cut -d ' ' -f2-)"
    pattern="-e '${first_tag}'"
    for v in $(echo $other_tags) ; do
        pattern="$pattern --and -e '$v'" 
    done
    echo "$pattern"
    
}



get_paper_info ()
{
    # Append only lines for given bookmark id
    sed  "/id: ${1}/,/^label: /!d;/^label: /q" $2 >> /tmp/papers-filtered.md
    printf '\n\n\n' >> /tmp/papers-filtered.md
}



filter_papers ()
{
    shift
    nb_tags="$(echo $@ | tr ' ' '\n' | wc -l)"
    if [[ "$nb_tags" -eq 1 ]]; then
        pattern="-e '$1'"
    else
        pattern=$(generate_git_grep_pattern "$@")
    fi

    bookmarks_ids="$(eval git --no-pager grep -h -B 1 $pattern --no-index $fp | rg '^id: ' | cut -d' ' -f2 | tr '\n' ' ' | sed 's/.$//')"

    # temporary empty file to receive filtered bookmarks
    :> /tmp/papers-filtered.md

    # Append info for each matching bookmard
    for b in $papers_ids ; do
        get_paper_info  $b $fp
    done

    # Generate new input file for fzf with only matching bookmarks
    fzf_paper_info /tmp/papers-filtered.md

}


add_tags ()
{
    current_tags=$(rg -NI '^tags: _\|#.*\|_' $HOME/notes/.papers.md | perl -ne 'print "$1\n" while /(#.*?)\|/g;' | sort -u)

    status="inprogress"
    tag_list=""
    while [ "$status" ]; do
        choice=$(echo "$current_tags" | fzf -m --bind alt-enter:print-query | tr '\n' ' ')
        if [[ -z "$choice" ]]; then
          status=""
        else
          tag_list="$tag_list$choice"
        fi
    done

    echo "$tag_list"
}



add_paper ()
{

    paper_id=$(date +%Y%m%d%H%M%S)
    doi="$(copyq clipboard)"

    # get metadata from crossref API
    # https://api.crossref.org/works/<doi>/
    metadata=$(curl -s https://api.crossref.org/works/${doi}/ \
        | jaq '.message')
    
    # Get info from json object
    title=$(echo "$metadata" | jaq '.title[0]' | tr -d '"')
    year=$(echo "$metadata" | jaq '.published["date-parts"][0][0]' | tr -d '"')
    journal=$(echo "$metadata" | jaq '.["container-title"][0]' | tr -d '"')
    authors=$(echo "$metadata" \
        | jaq '.author | map([.given, .family] | "\(.[0]) \(.[1])")' \
        | tr -d '"' | cat <(echo 'authors:') -)

    # Extract first author and create label
    first_author=$(echo "$authors" | head -n 3 | tail -n1 | tr -d ',"' | sed 's/^\s*//' | rev | cut -d' ' -f1 | rev)
    label="$first_author $year - $title"

    # add tags for paper
    tags="_|$(add_tags | tr ' ' '|')_"

    # Choose url or filepath to add
    gum format -- "## Choose type of link to add:"
    choice=$(gum choose "url" "filepath")
    if [[ "$choice" == "url" ]]; then
        link="$(copyq clipboard)"
    else
        link=$(ls -ltr $HOME/library/_files/papers/ | fzf)
    fi

    # format output
    cat << EOF >> $HOME/notes/.papers.md



id: $paper_id
label: $label
title: $title
year: $year
journal: $journal
$authors
doi: $doi
link: $link
label: $label
EOF
    
}



preview_bookmark ()
{
    fp="$1"

    awk -v target="$2" '
$0 ~ "^id: " target {
  flag=1
  next
}
flag && /^title: |^tags: / {
  printf $0 "\n\n"
  next
}
flag && /^url: / {
  printf "\n\n" $0
  flag=0
  next
}
flag {
  print $0
}
{
}
' "$fp"
    
}



open_bookmark ()
{
    chrome_window=$( ps -ax | grep google-chrome | wc -l)
    if [[ $chrome_window -eq 1 ]]; then
        dtach -n /tmp/chrome-dtach google-chrome "$1" > /dev/null 2>&1 &
        sleep 2s
        rm /tmp/chrome-dtach
    else
        google-chrome "$1" > /dev/null 2>&1 &
    fi
    # dtach_socket="/tmp/chrome-dtach"
    # if [[ ! -f "$dtach_socket" ]]; then
    #     dtach -n /tmp/chrome-dtach google-chrome "$1" > /dev/null 2>&1 &
    # else
    #     google-chrome "$1" > /dev/null 2>&1 &
    # fi
    
}



edit_bookmark ()
{
    # open bookmark with cursor on matching id
    nvim "+/id: $2" $1
}




remove_from_bookmarks ()
{

    # copy bookmarks to temporary file
    cp $HOME/notes/.bookmarks.md /tmp/bookmarks.md

    # remove target bookmark
    awk -v target="$1" '
/^<!--/ || $0 ~ "^id: " target {
  flag=1
  print $0
  next
}
flag && /^label: / {
  flag=0
  print $0 "\n"
  next
}
flag && /^-->$/ {
  flag=0
  print $0 "\n\n\n"
  next
}
flag {
  print $0
}
{
}
' /tmp/bookmarks.md > $HOME/notes/.bookmarks.md

    rm /tmp/bookmarks.md
    
}



fzf_bookmark_info ()
{

    cat $1 | awk '
/^id: |^url: /{
  printf substr($0, index($0,$2)) "~~~~~~~~"
  next
}
/^label: /{
  printf substr($0, index($0,$2)) "\n"
}
{
}
' > /tmp/bookmarks-fzf.md
    
}

bookmark_menu ()
{
    fzf_bookmark_info "$1"
    cat /tmp/bookmarks-fzf.md | fzf -m --preview-window=65% --delimiter='~{8}' --with-nth=3 \
        --preview="bk --preview $1 {1}" \
        --bind "ctrl-o:execute@bk --open {2}@" \
        --bind "alt-d:execute@bk --delete {1}@+reload(bk --fzf $1)" \
        --bind "alt-t:execute(bk --tags ${1} | sed 's/<placeholder>//g' | tr -d '~' | tr ' ' '\n' | gum filter --no-limit | bk --filter $1)+reload(cat /tmp/bookmarks-fzf.md)" \
        --bind "alt-r:execute@bk --fzf $1@+reload(cat /tmp/bookmarks-fzf.md)+show-preview" \
        --bind "ctrl-x:execute@bk --archive {1}@+execute@bk --fzf $1@+reload(cat /tmp/bookmarks-fzf.md)" \
        --bind "ctrl-e:execute@bk --edit $1 {1}@"
}

#------------------------------------------------------------------------------
#  Main program
#------------------------------------------------------------------------------

main () {

case "$1" in

  -t|--tags)
    papers_tags "$2"
    ;;

  -o|--open)
    open_paper "$2"
    ;;

  -d|--delete)
    remove_from_papers "$2"
    ;;

  -a|--add)
    add_paper
    ;;

  -e|--edit)
      edit_paper "$2" "$3"
    ;;

  -f|--fzf)
    fzf_paper_info "$2"
    ;;

  -F|--filter)
      fp=$2
      tags=$(while read -r t; do
          printf '%s ' $t
      done | sed 's/\s$//')
      filter_papers $fp $tags
    ;;

  -p|--preview)
    preview_paper "$2" "$3"
    ;;

  *)
    cd $HOME/notes
    papers_menu $HOME/notes/.papers.md
    ;;
esac

}

main "$@"
