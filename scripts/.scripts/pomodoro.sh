#!/bin/bash

##########################################
# POMODORO #SESSIONS:
#
# *) Without options:
#    - 1st argument = duration work
#    - 2nd argument = duration break
#    - 3rd argument = audio level (in %)
#
# *) Default argument values:
#    pomodoro 30m 5s 6
#
# *) Option "-i": fresh record file
#
##########################################

# First session of the day - empty record file

if [ "$1" == "-i" ]
then
  :>| ~/.config/mpv/pomodoro.txt
  shift # deletes first argument of "$@"
fi

# Set durations and volume
WORK="${1:-50m30s}"
BREAK="${2:-5m30s}"
VOLUME="${3:-6}"

# Work session - default 30min
countdown "$WORK" || exit

# Break
## Pause potential mpv work playlist - redirection output and error if none
echo '{"command": ["set_property", "pause", true]}' | socat - ~/.mpv/mpvsocket > /dev/null 2>&1

## Ensure volume not too loud
### Check whether headphones are plugged
#HEADPHONES=$(amixer -c 0 contents | \
#  grep -A 2 'numid=26' | \
#  tail -n 1 | \
#  cut -d "=" -f 2)
#if [ "$HEADPHONES" == "on" ]
#then
#  amixer -q set Master "$VOLUME"%
#else
#  amixer -q set Master 55%
#fi

## Launch mpv break playlist in background and record songs played in temporary file
#mpv --input-ipc-server=/tmp/mpvsocket --term-status-msg= --shuffle --playlist=$HOME/.config/mpv/playlist.txt | tee -a >/dev/null > /tmp/pomodoro.txt &

## Break countdown - default 5 min
countdown "$BREAK"

## Kill mpv instance with the break playlist
#kill -9 $(pgrep mpv | tail -n 1)
#echo '{"command": ["quit"]}' | socat - /tmp/mpvsocket > /dev/null 2>&1


## Clean mpv and append mpv output
#cat /tmp/pomodoro.txt <(printf '%s\n' "======") | grep -E '^\=|^\s.\w' >> ~/.config/mpv/pomodoro.txt
#cat ~/.config/mpv/pomodoro.txt

## Set volume not too loud and restart mpvsocket
#amixer -q set Master "$VOLUME"%
echo '{"command": ["set_property", "pause", false]}' | socat - ~/.mpv/mpvsocket > /dev/null 2>&1
