#!/bin/bash

##############################################
# AIM - Add song to 'practice_songs.csv' file
#
# Without any argument:
# 1) Open FZF
# 2) Take Target and Type fields as input
# 3) Format the input and song path and append
#    to csv file
# With -p option: print practice_songs.csv
##############################################


if [ "$1" == "-p" ]
then
  cat ~/documents/music/drums/practice_songs.csv | csvlook
elif [ "$1" == "-s" ]
then
  #Rscript ~/Scripts/sandbox/drumsongs.R
elif [ "$1" == "-e" ]
then
  vim ~/documents/music/drums/practice_songs.csv
else
  SONGPATH=$(fdfind . ~/Music/ | fzf) &&
  SONG=$(basename $SONGPATH)
  read -p "Enter Target/Type for the song: " input &&
  readsplit "," $SONGPATH $input > /tmp/drum-target.txt
  echo $(echo "${SONG%.*}" | cut -f 2- -d _ | tr _ ' ') > /tmp/drum-title.txt
  echo $SONGPATH | cut -f 5 -d / | tr _ ' ' > /tmp/drum-band.txt
  paste -d, /tmp/drum-target.txt /tmp/drum-band.txt /tmp/drum-title.txt >> ~/documents/music/drums/practice_songs.csv
fi
