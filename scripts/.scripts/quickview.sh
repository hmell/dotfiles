#!/bin/bash
case "$1" in
  -c)
    ls  ~/.cheatsheets/ |
    dmenu -l 10 -i      |
    while read -r file
    do
      zathura ~/.cheatsheets/$file &
    done
    #exit
  ;;
  *)
    vim -S ~/.vim/sessions/todo-log.vim
  ;;
esac



