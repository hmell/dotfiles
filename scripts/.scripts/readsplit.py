#!/usr/bin/env python3

"""AIM - Reparse input string containing tokens with spaces

INPUT
A sequence of arguments made of:
  i) a separator that has to be specified - e.g. ","
  ii) any number of tokens (single words or multiple words
  surrounded by double quotes)

OUTPUT
  A string containing each token separated by the field separator provided

EXAMPLES

readsplit "," "This is" a "test string"
--> This is,a,test string

readsplit ";" '"This is"' a "test string"
--> "This is";a;test string
"""

import sys
import shlex

separator = sys.argv[1]
words = sys.argv[2:]
# Escape backslash character in songs' path
words = [word.replace("\'", "\\'") for word in words]
data = shlex.split(" ".join(words))
sys.stdout.write(separator.join(data) + '\n')
