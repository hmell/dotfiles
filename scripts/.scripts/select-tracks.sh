#!/bin/bash
FILE="$1"
#echo $FILE >> ~/.config/mpv/mpv_playlist.txt
TRACKS_NB=$(simple-gui | awk -F' ' -v OFS="d;" '$1=$1') &&
find $(dirname "$FILE") -regextype posix-extended -regex ".*\.(mp3|flac|ape|m4a)$" |
sort |
sed "${TRACKS_NB}d" >> ~/.config/mpv/mpv_playlist.txt
