#!/bin/bash

wallpapers="$HOME/pictures/wallpapers"
configfile="$HOME/.config/nitrogen/backgrounds"

## Set default position to 1 for first call
pos=1
## Get path to current background from nitrogen config file
current="$(grep file ~/.config/nitrogen/bg-saved.cfg | cut -f2 -d=)"
#prevpos="$(grep $(basename $current) $configfile | cut -f1 )"

case "$1" in
  --inc)
    ## get the number of previous desktop and increment by 1
    prevpos="$(grep $(basename $current) $configfile | cut -f1)"
    pos=$(( $prevpos + 1 ))
  ;;
  --dec)
    ## get the number of previous desktop and decrement by 1
    prevpos="$(grep $(basename $current) $configfile | cut -f1)"
    pos=$(( $prevpos - 1 ))
  ;;
  *)
    ## Randomly select 3 pictures to use as wallpapers - for start of session
    ls $wallpapers | grep -v black | shuf -n 3 | nl > $configfile
  ;;
esac

background=${wallpapers}/$(grep "${pos}" $configfile | cut -f2)

if [ $pos -gt 1 -a $pos -lt 3 ]
  then
  nitrogen --set-auto ${wallpapers}/black.png 2> /dev/null
fi

case "$1" in
  --inc)
    ## keybinding to go to next dekstop in openbox
    xdotool set_desktop $prevpos 2> /dev/null # xdotools numbering for desktops starts at 0
  ;;
  --dec)
    ## keybinding to go to previous dekstop in openbox
    xdotool set_desktop $((  $prevpos - 2 )) 2> /dev/null # xdotools numbering for desktops starts at 0
  ;;
  *)
  ;;
esac

nitrogen --set-auto --save $background 2> /dev/null
