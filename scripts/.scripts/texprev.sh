#!/bin/bash

nb_texfiles="$(ls *tex | wc -l)"

if [ -z "$1" ] && [ "$nb_texfiles" -gt 1 ] ; then
    echo "Please specify which tex file to compile as first argument."
    exit 1
elif [ -z "$1" ] && [ "$nb_texfiles" -eq 1 ] ; then
    texfile="$(ls *.tex)"
else
    texfile="$1"
fi

fname="${texfile##*/}"
fname="${texfile%.*}"

pdflatex "$texfile"
rm "${fname}".{aux,log,out,toc} 2> /dev/null

google-chrome "${fname}.pdf" > /dev/null 2>&1 &
