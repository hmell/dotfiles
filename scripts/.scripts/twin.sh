#!/bin/bash
/home/hugo/.local/bin/tmux start
wmctrl -a Tmux || /home/hugo/.cargo/bin/alacritty -T "Tmux" -e \
    /home/hugo/.local/bin/tmux attach
