#!/bin/bash

JSON_DB="$HOME/notes/notes/.links.json"
NOTES_FOLDER="$HOME/notes/notes"

inotifywait -m $NOTES_FOLDER -e close_write -e delete |

    while read fpath action target; do

      # skip all actions for hidden files
      [[ "$target" =~ ^"." ]] && continue
      # skip all actions for sed temporary files
      [[ "$target" =~ ^"sed" ]] && continue


      echo "The file ${target} in directory ${fpath} was manipulated via ${action}\n" >> "$(dirname ${JSON_DB})/.log.txt"

      # add or update links between notes
      if [[ "$action" == 'CLOSE_WRITE,CLOSE' ]]; then

        NOTE_NO_EXT="${target%.*}"
        note_exists="$(jq --arg noteID "$NOTE_NO_EXT" 'has($noteID)' $JSON_DB)"

        if [[ "$note_exists" == "true" ]]; then
          wonotes --update $target
          printf 'On %s, the note %s was updated.\n' "$(date '+%c')" "$NOTE_NO_EXT" >> "$(dirname ${JSON_DB})/.log.txt"
        else
          wonotes --add $target
          printf 'On %s, the note %s was added.\n' "$(date '+%c')" "$NOTE_NO_EXT" >> "$(dirname ${JSON_DB})/.log.txt"
        fi

      # remove a note and all associated references
      else
        wonotes --delete $target
        printf '%s was deleted from database.\n' "$NOTE_NO_EXT" >> "$(dirname ${JSON_DB})/.log.txt"
      fi

    done
