#!/bin/bash

WIKI=$HOME/notes/notes

NBLINES=$(cat ${WIKI}/.target-list.txt | wc -l)
INPUT=$(paste -d '' <(printf '$HOME/notes/notes/\n%.0s' $(seq 1 $NBLINES)) <(cat ~/notes/notes/.target-list.txt))

if [ "$1" = '-a' ]
then
  INPUT="${WIKI}/*.md"
  shift 1
fi

if [ "$1" = '-d' ]
then
  shift 1
  # second sed call is used to add missing commas between labels as a result of previous deletion
  #printf '%s\n' $INPUT | xargs sed -i -E "s/,?\s?$1,?//; s/(tags:.*)' '#/\1', '/" |  sed -E "s/(tags:.*)' '#/\1', '/"
  printf '%s\n' $INPUT | xargs sed -i -E "s_,?\s?$1,?__;s_(tags:.*)' '#_\1', '_"
else
  printf '%s\n' $INPUT | xargs sed -i -E "s_$1_$2_"
fi




