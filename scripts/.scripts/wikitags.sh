#!/bin/bash
WIKI=$HOME/notes/notes

PATTERN="'#\w.*'"
INPUT="${WIKI}/*.md"

if [ "$1" = '-f' ]
then
  NBLINES=$(cat ${WIKI}/.list-of-files.txt | wc -l)
  INPUT=$(paste -d '' <(printf '$HOME/notes/notes/\n%.0s' $(seq 1 $NBLINES)) <(cat ~/notes/notes/.list-of-files.txt))
elif [ "$1" = '-g' ]
then
  INPUT=$(rg --files-with-matches "$2" $WIKI)
fi

rg -o -N --no-filename --no-heading -e "$PATTERN" $INPUT |
#sed -r "s:[\s'#]::g" |
sed -r "s:,\s:\n:g" |
sort |
uniq
