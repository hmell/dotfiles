#!/bin/bash

JSON_DB="$HOME/notes/notes/.links.json"
NOTES_FOLDER="$HOME/notes/notes"

NOTE_FILENAME="$2"
NOTE_NO_EXT="${2%.*}"

#------------------------------------------------------------------------------
#   FUNCTIONS
#------------------------------------------------------------------------------


# init_note <note_id>
# Initialize a new note object in JSON database for given ID
# args:
#  - $1: ID of new note (e.g. "202201281324361") used as key for note object
init_note () 
{
  local note="$1"
  local tmp=$(mktemp)
  jq --arg noteID "$note" '.[$noteID] = { "linksTo": [], "linkedBy": []}' $JSON_DB > "$tmp" && mv "$tmp" $JSON_DB
}

 
# grep_links <note_filename>
# Given a note filename, prints all unique links referencing other notes 
# args:
#  - $1: note filename
grep_links ()
{
  local note="$1"
  rg --color=never "\[.*\]\(([0-9]{14})\)" -or '$1' ${NOTES_FOLDER}/$note | sort | uniq | tr '\n' ' ' | sd '\s$' ''
}


# update_links_to <note_id> "<json_array_of_links>"
# Add list of unique links referencing other notes
# args:
#  - $1: key of note object to update
#  - $2: string with JSON array containing each link 
#        (e.g. var='["20212203...", "20229306076...", ... ]')
update_links_to ()
{
  local note="$1"
  local links_json="$2"
  local tmp=$(mktemp)
  jq --arg noteID "$note" --argjson arr "$links_json" '.[$noteID].linksTo = $arr' $JSON_DB > "$tmp" && mv "$tmp" $JSON_DB
}

# remove_links_to <target_note_id> <link_ID>
# Remove a note ID to "linkTo" array of a target note
# args:
#  - $1: key of target note object
#  - $2: string to remove from "linkedBy" array
remove_links_to ()
{
  local target="$1"
  local obsolete_link="$2"
  local tmp=$(mktemp)
  jq --arg noteID "$target" --arg elem "$obsolete_link" 'del(.[$noteID].linksTo[] | select(. == $elem))' $JSON_DB > "$tmp" && mv "$tmp" $JSON_DB
}


# add_linked_by <target_note_id> <link_ID>
# Push new link to "linkedBy" array of a target note
# args:
#  - $1: key of target note object
#  - $2: string to add to "linkedBy" array
add_linked_by ()
{
  local target="$1"
  local new_link="$2"
  local tmp=$(mktemp)
  jq --arg noteID "$target" --arg elem "$new_link" '.[$noteID].linkedBy += [$elem]' $JSON_DB > "$tmp" && mv "$tmp" $JSON_DB
}


# remove_linked_by <target_note_id> <link_ID>
# Remove a note ID to "linkedBy" array of a target note
# args:
#  - $1: key of target note object
#  - $2: string to remove from "linkedBy" array
remove_linked_by ()
{
  local target="$1"
  local obsolete_link="$2"
  local tmp=$(mktemp)
  jq --arg noteID "$target" --arg elem "$obsolete_link" 'del(.[$noteID].linkedBy[] | select(. == $elem))' $JSON_DB > "$tmp" && mv "$tmp" $JSON_DB
}


# update_linked_by <link_id> <ids_link_addition> <ids_link_removal> 
# Respectively add or remove link in "linkedBy" of lists of notes
# args:
#  - $1: link ID to add/remove depending on context
#  - $2: space separated list of IDs where link should be added to "linkedBy"
#  - $3: space separated list of IDs where link should be removed from "linkedBy"
update_linked_by ()
{
  local linkID="$1"
  local ids_for_addition="$2"
  local ids_for_removal="$3"

  [[ ! -z "$ids_for_addition" ]] &&
    for i in $ids_for_addition ; do
      add_linked_by "$i" "$linkID"
    done

  [[ ! -z "$ids_for_removal" ]] && 
    for i in $ids_for_removal ; do
      remove_linked_by "$i" "$linkID"
    done
}


# get_new_links <note_id> <links_array>
# Get links in new version only
# args:
#  - $1: key of note object to check
#  - $2: unique links in current version of note as json array
get_new_links ()
{
  local note="$1"
  local arr_new="$2"
  local arr_old="$(jq --arg noteID "$note" '.[$noteID].linksTo' $JSON_DB )"
  echo -n "{\"a\": ${arr_new}, \"b\": ${arr_old}}" | jq '.a - .b'
}


# get_obsolete_links <note_id> <links_array>
# Get links no longer in new version
# args:
#  - $1: key of note object to check
#  - $2: unique links in current version of note as json array
get_obsolete_links ()
{
  local note="$1"
  local arr_new="$2"
  local arr_old="$(jq -c --arg noteID "$note" '.[$noteID].linksTo' $JSON_DB )"
  echo -n "{\"a\": ${arr_new}, \"b\": ${arr_old}}" | jq -c '.b - .a'
}


# fmt_links_as_json_array <list_of_links>
# Convert space separated list to json array
# args:
#  - $1: space separated list of links
fmt_links_as_json_array ()
{
  local list="$1"
  echo -n [\"$(printf '%s' "$list" | sd '\s' '", "')\"]  
}


# fmt_links_as_raw_list <links_array>
# Convert json array to space separated list
# args:
#  - $1: json array of links
fmt_links_as_raw_list ()
{
  local links_array="$1"
  echo -n $links_array | tr -d '[" ]' | tr ',' ' '
}


# diff_contains_links <note_filename>
# Returns 0 if diff of note includes note links, 1 otherwise
# args:
#  - $1: note filename
diff_contains_links ()
{
  local note="$1"
  git -C $NOTES_FOLDER diff $note | rg --quiet "^\+.*\[.*\]\([0-9]{14}\).*"
  echo -n $?
}

# display_links <flag_for_link_type> <note_id>
# Display content of "linksTo" or "linkedBy" arrays for given note object
# args:
#  - $1: "--links-to" or "--linked-by" flag to specify type of link to display
#  - $2: note ID
display_links ()
{
  local flag="$1"
  local note="$2"

  if [[ $flag == "--links-to" ]]; then
    local linksTo="$(jq -c --arg noteID "$note" '.[$noteID].linksTo' $JSON_DB | tr -d '[]"' | tr ',' ' ')"
    printf '%s\n' $linksTo
  else
    local linkedBy="$(jq -c --arg noteID "$note" '.[$noteID].linkedBy' $JSON_DB | tr -d '[]"' | tr ',' ' ')"
    printf '%s\n' $linkedBy
  fi
}


# delete_note <note_id>
# Remove a note from json database and all related links
delete_note ()
{
  local note="$1"
  local target_notes="$(fmt_links_as_raw_list $(get_obsolete_links $note '[]'))"

  if [[ ! -z $target_notes ]]; then
    update_linked_by $note "" "$target_notes"
  fi

  # remove link in notes that links to deleted notes
  local links_target_array="$(jq -c --arg noteID "$note" '.[$noteID].linkedBy' $JSON_DB )"
  local links_target_list="$(fmt_links_as_raw_list "$links_target_array")"
  [[ ! -z "$links_target_list" ]] &&
    for i in $links_target_list ; do
      remove_links_to "$i" "$note"
      [[ -f "${NOTES_FOLDER}/${i}.md" ]] && ipath="${i}.md" || ipath="${i}.Rmd"
      sed -i "s/\[\](${note})/\[\](--\!${note}\!--)/g" $ipath
    done
 
  # remove note object from JSON database
  local tmp=$(mktemp)
  jq --arg noteID "$note" 'del(.[$noteID])' $JSON_DB > "$tmp" && mv "$tmp" $JSON_DB
}

#------------------------------------------------------------------------------
#   COMMAND PARSING AND EXECUTION
#------------------------------------------------------------------------------

case "$1" in

  --init)

    init_note "$NOTE_NO_EXT"
    ;;

  --add)

    # get all unique links from the new note
    links_list="$(grep_links "$NOTE_FILENAME")"

    # add links references to focal and target notes
    if [[ ! -z $links_list ]]; then
      links_array=$(fmt_links_as_json_array "$links_list")
      update_links_to "$NOTE_NO_EXT" "$links_array"
      update_linked_by "$NOTE_NO_EXT" "$links_list" ""
    fi
  ;;

  --update)

    # check note links in modification saved
    if [[ $(diff_contains_links $NOTE_FILENAME) -eq 1 ]]; then
      exit ;
    fi

    # get unique links in new version
    links_list="$(grep_links "$NOTE_FILENAME")"
    links_array=$(fmt_links_as_json_array "$links_list")

    # update links of focal note
    update_links_to "$NOTE_NO_EXT" "$links_array"

    # update links of target notes
    links_for_addition="$(fmt_links_as_raw_list $(get_new_links "$NOTE_NO_EXT" "$links_array") )"
    links_for_removal="$(fmt_links_as_raw_list $(get_obsolete_links "$NOTE_NO_EXT" "$links_array") )"
    update_linked_by "$NOTE_NO_EXT" "$links_for_addition" "$links_for_removal"
  ;;

  --delete)

    delete_note "$NOTE_NO_EXT"
  ;;

  --display-links-to)

    display_links --links-to $NOTE_NO_EXT
  ;;

  --display-linked-by)

    display_links --linked-by $NOTE_NO_EXT
  ;;

  *)

    jq '.' $JSON_DB
  ;;
  
esac
