#!/bin/bash
main_bibfile="$HOME/library/references.bib"

# Get unique bibkeys and join with '\|' to form regexp
bibkeys_regexp=$(rg --no-filename -o "[\s\[\"'(\{]@[[:alnum:]]+?[\s\]\"')},;]" "$1" \
  | sort \
  | uniq \
  | tr '\n' ',' | sed 's/,$//; s/[@"]//g; s/,/\\|/g')

# 'refs/' subdirectory at md file location if absent
dirMdFile=$(dirname $(realpath "$1"))
[[ -d "${dirMdFile}/refs" ]] || mkdir "${dirMdFile}/refs"

# Extract to 'refs/refs.bib' entries matching bibkeys in the regex
bibtool -X "$bibkeys_regexp" "$main_bibfile" -o "${dirMdFile}/refs/refs.bib" 

# TODO: match positional arguments with case to decide whether we should also
# output refs.json and a refs.html respectively (e.g. -h flag for html and -j
# flag for json)
