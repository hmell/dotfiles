# Tmux plugin manager and tmux plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'sunaku/tmux-navigate'
set -g @plugin 'tmux-plugins/tmux-resurrect' # C-s (save) - C-r (restore)
set -g @plugin 'tmux-plugins/tmux-continuum'

set -g @resurrect-processes 'copyq "copyq->copyq &"'
set -g @continuum-restore 'on'


# prefix remapping
set -g prefix C-space
bind Space send-prefix

# source config file
bind-key r source-file ~/.tmux.conf \; display-message "Reloaded config"

set -g default-terminal "screen-256color"

## Status line
# status bar
set -g status-right "#(/home/hugo/.local/bin/pomo show)  | #(/home/hugo/.scripts/pwbattery.sh)  "

set -g status-style "fg=#665c54"
set -g status-left-style "fg=#928374"

set -g status-bg default
set -g status-position top
set -g status-interval 1
# set -g status-right-length 90


#set -g status-interval 1
#set -g status-right "#(pomo)"
#set -g status-style fg=colour246,bg=colour238
#setw -g window-status-current-style fg=black,bg=colour68

bind-key Q set-option status

## No Mouse
set-option -g mouse on

set-option -g detach-on-destroy off

#---------------------
#- WINDOWS - PANES ---
#---------------------

# Pane Splitting
  #split but also retain current directory of existing path
bind '_' split-window -v -c "#{pane_current_path}"
bind '!' split-window -h -c "#{pane_current_path}"
bind 'C-C' new-window -c "#{pane_current_path}"

set -g main-pane-width '70%'

# Pane navigation
# see https://github.com/sunaku/tmux-navigate
set -g @navigate-left  '-n C-h'
set -g @navigate-down  '-n C-j'
set -g @navigate-up    '-n C-k'
set -g @navigate-right '-n C-l'
set -g @navigate-back  '-n C-\'

# Pane borders and background depending on focus
set -g window-style 'fg=colour247,bg=colour236'
set -g window-active-style 'fg=default,bg=colour235'
set -g pane-border-style 'bg=colour236,fg=colour238'
set -g pane-active-border-style 'bg=default,fg=cyan'

# switch off auto-renaming of windows
set-window-option -g automatic-rename off
set-option -g allow-rename off

# renumber windows after deletion
set-option -g renumber-windows on

# toggle zoom remapping
bind -n M-z resize-pane -Z

# reorder windows
bind -r C-w swap-window -d -t -1
bind -r C-x swap-window -d -t +-1

#----------------
#- COPY/PASTE ---
#----------------

## Use vim keybindings in copy mode
setw -g mode-keys vi
set-option -s set-clipboard off
bind P paste-buffer
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi y send-keys -X rectangle-toggle
unbind -T copy-mode-vi Enter
bind-key -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel 'xclip -se c -i'
bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'xclip -se c -i'


#------------------------
#- SESSIONS MANAGEMENT ---
#------------------------

# keybindings to jump between sessions and panes
bind C-j switch-client -t home:shell
bind C-k if-shell 'tmux has-session -t "-*"' 'switch-client -t "-*"' '' 
bind C-l new-window -n calendar "khal calendar -a home ; sleep 30s"
bind M-l new-window -n calendar-edit "khal interactive -a home"
bind M-h display-popup -E "$HOME/.scripts/khal.sh --hell"
bind T new-window -n todo "nvim ~/notes/notes/20230107152401.md"
# bind C-l if-shell 'tmux has-session -t calendar' 'switch-client -t calendar' 'run-shell -b $HOME/.tmux/sessions/calendar'
bind C-n if-shell 'tmux has-session -t notes' 'switch-client -t notes' 'run-shell -b $HOME/.tmux/sessions/notes'
bind C-m if-shell 'tmux has-session -t music' 'switch-client -t music' 'run-shell -b $HOME/.tmux/sessions/music'
bind C-s display-popup -E "$HOME/.tmux/sessions/$(ls $HOME/.tmux/sessions/ | $HOME/.fzf/bin/fzf --reverse)"
bind P display-popup -E "$HOME/.config/bin/tt.sh"
bind j {
    if-shell "v=0" {
        run-shell -b 'v=0' ; display-popup -E "tmux list-sessions\| sed -E 's/:.*$//' | grep -v \"^$(tmux display-message -p '#S')\$\" | $HOME/.fzf/bin/fzf --reverse | xargs -r tmux switch-client -t"
    } {
        display-popup -E "tmux list-sessions | sed -E 's/:.*$//' | grep -v \"^$(tmux display-message -p '#S')\$\" | $HOME/.fzf/bin/fzf --reverse | xargs -r tmux switch-client -t "} 
}
bind k display-popup -E "tmux list-windows | cut -f2 -d ' ' | sed  -E 's/-$//' | grep -v \"\*\$\" | $HOME/.fzf/bin/fzf --reverse | xargs -Iid -r tmux switch-client -t :id" 
bind-key C-P if-shell "v=0" "run-shell -b 'v=0' ; run-shell -b $HOME/.config/bin/tmux-switch-pane.sh" "run-shell -b $HOME/.config/bin/tmux-switch-pane.sh"

bind -n M-L switch-client -l

#--------------------------------------------------------
#- Quick access to specific files, documents or links ---
#--------------------------------------------------------

# bind C-b if-shell "v=0" "run-shell -b 'fd -g ueberzugpp-*.socket /tmp -x ueberzugpp cmd -s {} -a remove' ; display-popup -E -w 80% -h 80% \"bash -c '$HOME/.scripts/bookmarks/bookmarks.sh'\"" "display-popup -E -w 80% -h 80% \"bash -c '$HOME/.scripts/bookmarks/bookmarks.sh'\""
# important config files
bind -n M-P if-shell "v=0" "run-shell -b 'v=0' ; display-popup -E $HOME/.config/bin/portal.sh" "display-popup -E $HOME/.config/bin/portal.sh"
# select and open pdfs in library
bind-key C-q new-window -n quick-docs "$HOME/.config/bin/qdocs.sh"
# select and open important notes
# bind-key C-q new-window -n quick-notes "$HOME/.config/bin/qnotes.sh"
# quick access to library logs
# bind-key L new-window -n lib-logs "$HOME/.config/bin/liblog.sh"

# snippets, cheatsheets, keybindings
bind M-x new-window -a -n snippets "nvim -c ':QuickSearch \"#snippets::\"'"
bind M-c new-window -a -n cheatsheets "nvim -c ':QuickSearch \"#cheatsheets::\"'"
bind M-k new-window -a -n keybindings "nvim -c ':QuickSearch \"#keybindings::\"'"

# bookmarks of urls
bind C-b new-window -n bookmarks "/home/hugo/.scripts/bk.sh"
bind M-b new-window -n bookmarks "printf 'url\ndoi\nisbn' | gum filter | xargs /home/hugo/.scripts/bk.sh -a"
# bind C-b if-shell new-window -n lib-logs "v=0" "run-shell -b 'v=0' ; display-popup -E -w 80% -h 80% \"bk \"" "display-popup -E -w 80% -h 80% \"bk\""


#----------------------------------------------
#- Keybindings related to specific programs ---
#----------------------------------------------

# jrnl
# bind-key C-w new-window -n jrnl-tags -- zsh -i -c 'copyq copy "$(jrnl --tags | fzf -m --cycle | cut -d:  -f1 | xargs echo)"'

#--------------------#
#- STARTUP SESSION  -#
#--------------------#

if-shell 'tmux has-session -t home' {
  run-shell "echo no execution for config reloading > /dev/null"
  }{
  new-session -n shell -s home -c $HOME
  # send-keys -t home:shell 'copyq &' C-m
  selectw -t 0
}

# cht.sh easy use (see wiki)
#bind-key i run-shell "tmux neww ~/.scripts/cht.sh"


# --------------------------

run '/home/hugo/.tmux/plugins/tpm/tpm'
