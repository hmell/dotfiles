#!/bin/bash

rootdir="$HOME/code/_sandbox/scratchpad"
scratchpads=(bash python R web)
target=$(printf '%s\n' "${scratchpads[@]}" | fzf --reverse)

# check whether scratchpad already opened
has_scratchpad ()
{
  tmux has-session -t "scratchpad:$1" > /dev/null
  echo $?
}

#----------------------------------------------------------
#   Functions to setup scratchpad sessions
#----------------------------------------------------------

# bash scratchpad
new_bash_scratchpad ()
{
  # session layout
  tmux split-window -h -c "${rootdir}/bash" -t scratchpad:bash

  # sending keys in specific panes
  tmux send-keys -t scratchpad:bash.1 'nvim scratchpad.sh' C-m
  tmux select-pane -t scratchpad:bash.1
}

# python scratchpad
new_python_scratchpad ()
{
  # session layout
  tmux split-window -h -c "${rootdir}/python" -t scratchpad:python

  # sending keys in specific panes
  tmux send-keys -t scratchpad:python.1 'nvim scratchpad.py' C-m
  tmux send-keys -t scratchpad:python.2 'ipython' C-m
}


# R scratchpad
new_R_scratchpad ()
{
  # session layout
  tmux split-window -h -c "${rootdir}/R" -t scratchpad:R

  # sending keys in specific panes
  tmux send-keys -t scratchpad:R.1 'nvim scratchpad.R' C-m
  tmux send-keys -t scratchpad:R.2 'R' C-m
}

# web developpement scratchpad
new_web_scratchpad ()
{
  # session layout
  tmux split-window -c "${rootdir}/web" -vb -p 80  -t scratchpad:web

  # sending keys in specific panes
  tmux send-keys -t scratchpad:web.1 'nvim index.html styles.css index.js' C-m
  tmux send-keys -t scratchpad:web.2 'live-server .' C-m
}

#----------------------------------------------------------
#   COMMAND EXECUTION
#----------------------------------------------------------

target_found=$(has_scratchpad $target)

if [[ $target_found -eq 0 ]]; then
  tmux switch -t scratchpad:$target
else
  tmux new-session -Ad -s scratchpad -c ${rootdir}/bash
  [[ $target == "bash" ]] || tmux new-window -c "${rootdir}/$target" -n $target -t scratchpad
  new_${target}_scratchpad
  tmux switch -t scratchpad
fi
