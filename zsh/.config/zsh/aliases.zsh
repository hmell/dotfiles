alias k="clear"
alias ex="exit"
alias lsf='find $(pwd) -name '
alias psa="pomo start"
alias pso="pomo stop"
alias pse="pomo var edit"
alias v="nvim"
alias vm="export NVIM_APPNAME='nvim-minimal'; nvim"
alias t="tmux attach"
alias qnv="XDG_CONFIG_HOME=$HOME/.config/quarto-nvim nvim"
alias xo="xdg-open"
alias fzm="fzf-music"
alias R="R --no-save" # avoid prompt to save workspace
alias dropbox="$HOME/.dropbox-dist/dropboxd"
alias lg="lazygit"
alias pwbattery="$HOME/.scripts/pwbattery.sh"
alias fnap='nap $(nap list | gum filter) | copyq copy -'
alias pnap='nap list | fzf | xargs -I {} nap "{}" | gum format | gum pager --show-line-numbers=false --border=hidden'
alias kd="$HOME/.scripts/kukariri.sh"
alias kspa="$HOME/.scripts/kukariri.sh --spanish"
alias kdt="$HOME/.scripts/kukariri.sh -t"
alias ktspa="$HOME/.scripts/kukariri.sh -t --spanish"
alias mdw='mv "/mnt/c/Users/hugo7/Downloads/$(echo $(ls /mnt/c/Users/hugo7/Downloads/ | gum filter))" /home/hugo'
alias kh='khal interactive -a hell'
alias mp='mpv --playlist=/home/hugo/music/THE_playlist.txt --loop-playlist=inf'


# Aliases for git
alias gs="git status"
alias ga="git add"
alias gc="git commit"
## dotfiles repo
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles_bak/ --work-tree=$HOME'
alias ds='/usr/bin/git --git-dir=$HOME/.dotfiles_bak/ --work-tree=$HOME status'
alias da='/usr/bin/git --git-dir=$HOME/.dotfiles_bak/ --work-tree=$HOME add'
alias dc='/usr/bin/git --git-dir=$HOME/.dotfiles_bak/ --work-tree=$HOME commit'
alias dlg="lazygit --git-dir=$HOME/.dotfiles_bak --work-tree=$HOME"

# aliases for python
alias ae='deactivate &> /dev/null; source ./venv/bin/activate'
alias de='deactivate'
alias ce='python -m venv ./venv'

# aliases for calcurse
alias gcal='calcurse -G --filter-pattern'

# aliases for scripts
alias texprev='/home/hugo/.scripts/texprev.sh'
alias texeq='pdf-crop-margins -v -p 0 -a -6 scratchpad.pdf && pdftoppm -png scratchpad_cropped.pdf tex_equation.png && rm scratchpad*.pdf && mv tex_equation*.png $HOME/tex_equation_000.png'

# aliases for containers
alias podgo='podman run --rm -it -v "$(pwd):/root/project" "devcontainer-go:latest"'
alias podr='podman run --rm -it -p 127.0.0.1:8686:8686 -p 127.0.0.1:8888:8888 -v "$(pwd):/root/project" "devcontainer-r:latest"'
alias podpy='podman run --rm -it -v "$(pwd):/root/project" "devcontainer-py:latest"'
# alias pod-bayes='podman run --rm -it -p 8888:8888 -e DISABLE_AUTH=true -p 127.0.0.1:8787:8787 -v $(pwd):/home/root/project docker.io/hugomell/bayes-cli:4.3.1'
#alias pod-bayes='podman run --rm -it -p 0.0.0.0:8888:8888 -p 0.0.0.0:4848:4848 -e DISABLE_AUTH=true -p 127.0.0.1:8787:8787 -v $(pwd):/home/root/project r2u-bayes:latest'
#alias pod-py='podman run --rm -it -v $(pwd):/home/root/project python-ds:3.12 ipython'
#alias pod-r2u='podman run --rm -it -p 0.0.0.0:4848:4848 -e DISABLE_AUTH=true -p 127.0.0.1:8787:8787 -v $(pwd):/home/root/project r2u-core:latest'
