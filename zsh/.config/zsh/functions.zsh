fjrnl ()
{
	case "$1" in
		"-o")
		# open entries with selected tags (OR mode)
		jrnl $(jrnl --tags | fzf -m --cycle | cut -d' ' -f1 ) --edit
		;;
		"-t")
		# copy selected tags to clipboard
		jrnl --tags | fzf -m --cycle | cut -d' ' -f1 | xargs echo | xclip -selection "clipboard" -i
		;;
		*)
		# open entries with selected tags (AND mode)
		jrnl $(jrnl --tags | fzf -m --cycle | cut -d' ' -f1 | tr '\n' ' ' | sed 's/.$//' | sed 's/\s/ -and /g') --edit

# tmux new-window -n jrnl-tags 'copyq copy "$(jrnl --tags | fzf -m --cycle | cut -d:  -f1 | xargs echo)"'

# tmux new-window -n jrnl-tags zsh -c 'copyq copy "$(echo hello there @tag/hfouehf)"'

		;;
	esac
}

bibsub ()
{
    bibtex-ls | \
        fzf --multi --ansi --cycle --reverse | \
        rg -o "@\w+" | tr -d "@" | tr '\n' '|' | \
        sed  "s/^/'/;s/.$//;s/$/'/;s/|/\\\|/g" | \
        xargs -I{} bibtool -X {} -o bibsub.bib $HOME/library/_files/references.bib
}



m ()
{
$HOME/.scripts/musiclib.sh albums | fzf -e --multi --ansi --cycle --reverse \
    --bind="alt-a:select-all,alt-d:deselect-all" | \
    sd " \| " "/" | sd " " "_" | sd "^" "/home/hugo/music/" | head -n -1 | \
    xargs mpv --audio-display=no
}



mt ()
{
$HOME/.scripts/musiclib.sh titles | fzf -e --no-sort --multi --ansi --cycle --reverse \
    --bind="alt-a:select-all,alt-d:deselect-all" | \
    sd " \| " "/" | sd " " "_" | sd "^" "/home/hugo/music/" | head -n -1 | \
    xargs mpv --audio-display=no
}
